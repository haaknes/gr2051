package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Paths;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;

import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import frisbeegolf.json.ReadWrite;
import fxmapcontrol.Location;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NewCourseIT extends ApplicationTest {

  private NewCourseController controller;
  private ReadWrite rw = new ReadWrite();

  @Override
  public void start(final Stage stage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(getClass().getResource("NewCourse_test.fxml"));
    controller = new NewCourseController(new Location(1.111111, 2.222222));
    loader.setController(controller);
    final Parent root = loader.load();
    stage.setScene(new Scene(root));
    stage.show();
  }

  @BeforeEach
  public void checkServer() {
    assertNotNull(System.getProperty("frisbeegolf.port"));

    Course course = new RemoteFrisbeegolfAccess().getCourse("63_402217s10_440188");
    assertNotNull(course);
  }

  @Test
  public void testMakeNewCourse() {
    clickOn("#nameField").write("New Course");

    VBox holeContainer = lookup("#holeContainer").query();
    interact(() -> {
      holeContainer.getChildren().remove(0);
      holeContainer.getChildren().add(new NewHoleComponentMock());
    });

    clickOn("#saveButton");

    Button button = lookup("#startButton").query();
    assertEquals("Start", button.getText(), "Did not get expected button on main menu");

    Course remoteCourse = rw.readCourse("1_111111s2_222222");
    if (remoteCourse != null) {
      assertEquals("1_111111s2_222222", remoteCourse.getId(),
          "Generated course did not have expected id");
      assertEquals("New Course", remoteCourse.getName(),
          "Generated course did not have expected name");
      assertEquals(1, remoteCourse.getNumberOfHoles(),
          "Generated course did not have expected number of holes");
    } else {
      fail("Course was not written to file on server");
    }

    Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses", "1_111111s2_222222.json")
        .toFile().delete();
  }

  public class NewHoleComponentMock extends NewHoleComponent {
    @Override
    public boolean checkForm(Label messageLabel) {
      return true;
    }

    @Override
    public Hole getHole() {
      return new Hole(3, 57.3, "Short");
    }
  }
}
