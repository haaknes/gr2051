package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;
import frisbeegolf.json.ReadWrite;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class SimulateUserIT extends ApplicationTest {
  private static Date date;

  @Override
  public void start(final Stage stage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(AppController.class.getResource("Menu.fxml"));
    final Parent root = loader.load();
    stage.setScene(new Scene(root));
    stage.show();
  }

  @BeforeAll
  private static void setTime() {
    date = new Date();
  }

  /**
   * This test simulates a user that plays a game with a friend. He does some throws, and wants to
   * play on the same scorecard later.
   */
  @Test
  public void testSimulatePlayGameWithFriends() {
    clickOn("#startButton");
    WaitForAsyncUtils.waitForFxEvents();
    clickOn("#63_402217s10_440188");
    WaitForAsyncUtils.waitForFxEvents();
    clickOn("#playerList");
    WaitForAsyncUtils.waitForFxEvents();
    clickOn("#per");
    WaitForAsyncUtils.waitForFxEvents();
    clickOn("#playGame");
    WaitForAsyncUtils.waitForFxEvents();
    for (Node node : lookup("#addButton").queryAll()) {
      clickOn(node);
      WaitForAsyncUtils.waitForFxEvents();
    }
    clickOn("#clickBack");
    WaitForAsyncUtils.waitForFxEvents();
    clickOn("#earlierButton");
    WaitForAsyncUtils.waitForFxEvents();
    WaitForAsyncUtils.waitForFxEvents();
    assertEquals("Spilte med: Per", ((Label) lookup("#players2").query()).getText(),
        "Did not play with the expected player");
    assertEquals("Kast:  1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ",
        ((Label) lookup("#score").query()).getText(),
        "Did not get expected number of throws on the different holes");

  }

  @AfterAll
  private static void delete() {
    ReadWrite rw = new ReadWrite();
    List<String> files = rw.listFiles("scorecards");
    Pattern pattern = Pattern.compile("[0-9]");
    for (String file : files) {
      Matcher matcher = pattern.matcher(file);
      if (matcher.find()) {

        String number = file.substring(matcher.start());
        if (number.compareTo("" + date.getTime()) >= 1) {
          Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", file + ".json")
              .toFile().delete();
        }
      }
    }
  }
}
