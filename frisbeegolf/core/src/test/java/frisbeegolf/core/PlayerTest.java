package frisbeegolf.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerTest {

  private Player player;

  @BeforeEach
  public void setUp() {
    player = new Player("Bob");
  }

  @Test
  public void testConstructor() {
    Collection<String> friendsList = new ArrayList<>();
    String a = "Jon";
    friendsList.add(a);
    String b = "Tore";
    friendsList.add(b);
    String id = "testId";

    Player player = new Player("testName", id, friendsList);

    assertEquals(2, player.getFriends().size(), "Not expected number of friends");
    assertEquals("testName", player.getName());
    assertEquals(id, player.getId());
    assertEquals(friendsList, player.getFriends());

  }


  @Test
  public void testAddFriends() {
    String a = "Bjarne";
    String b = "Lars";
    Collection<String> friends = new ArrayList<>();
    friends.add(a);
    friends.add(b);

    player.addFriends(friends);

    assertEquals(2, player.getFriends().size(), "Not expected number of friends");
    assertTrue(friends.containsAll(player.getFriends()));
  }

  @Test
  public void testGetFriends() {
    String a = "Bjarne";
    String b = "Lars";
    Collection<String> friends = new ArrayList<>();
    friends.add(a);
    friends.add(b);

    player.addFriends(friends);

    assertEquals(2, player.getFriends().size(), "Not expected number of friends");
    assertTrue(friends.containsAll(player.getFriends()));
  }

  @Test
  public void testSetName() {
    player.setName("testname");
    assertEquals("testname", player.getName());
    player.setName("testname2");
    assertEquals("testname2", player.getName());
  }

  @Test
  public void testSetId() {
    player.setId("testId");
    assertEquals("testId", player.getId());
  }
}
