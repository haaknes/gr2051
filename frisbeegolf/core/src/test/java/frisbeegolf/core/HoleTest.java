package frisbeegolf.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class HoleTest {

    @Test
    public void testConstructor_parLength() {
        Hole hole = new Hole(3, 76);

        assertEquals(3, hole.getPar());
        assertEquals(76, hole.getLength());
        assertEquals("", hole.getDescription());
    }

    @Test
    public void testConstructor_parLengthDescription() {
        Hole hole = new Hole(4, 205, "OB in water");

        assertEquals(4, hole.getPar());
        assertEquals(205, hole.getLength());
        assertEquals("OB in water", hole.getDescription());
    }

    @Test
    public void testConstructor_negativePar() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Hole(-3, 56);
        });
    }

    @Test
    public void testConstructor_negativeLength() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Hole(5, -327);
        });
    }
}
