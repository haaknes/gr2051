package frisbeegolf.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CourseTest {

  private List<Hole> holeList;
  private Course course;

  @BeforeEach
  public void setUp() {
    holeList = new ArrayList<>();
    course = new Course("courseId", "testName", holeList);
  }

  @Test
  public void testConstructor_holeList() {
    Hole h = new Hole(3, 57);
    holeList.add(new Hole(3, 102));
    holeList.add(h);
    holeList.add(new Hole(4, 168));
    Course course = new Course("courseId", "A disc golf course", holeList);

    assertEquals(3, course.getHoles().size(), "Not expected number of holes");
    assertEquals(h, course.getHole(2));
  }

  @Test
  public void testConstructor_varargs() {
    Hole h1 = new Hole(3, 89);
    Hole h2 = new Hole(3, 89);
    Course course = new Course("courseId", "testName", Arrays.asList(h1, h2));
    assertEquals(2, course.getHoles().size(), "Not expected number of holes");
    assertEquals(h1, course.getHole(1));
  }

  @Test
  public void testAddHole() {
    Hole h = new Hole(3, 89);

    course.addHole(h);

    assertEquals(1, course.getHoles().size());
    assertEquals(h, course.getHole(1));
  }

  @Test
  public void testRemoveHole() {
    Hole h1 = new Hole(3, 89);
    Hole h2 = new Hole(3, 89);

    course.addHole(h1);
    course.addHole(h2);

    assertEquals(2, course.getHoles().size());

    course.removeHole(1);

    assertEquals(1, course.getHoles().size());
    assertEquals(h2, course.getHole(1));
  }

  @Test
  public void testGetHole() {
    Hole h1 = new Hole(4, 189);
    Hole h2 = new Hole(3, 52);

    course.addHole(h1);
    course.addHole(h2);

    assertThrows(IllegalArgumentException.class, () -> {
      course.getHole(0);
    });
    assertThrows(IllegalArgumentException.class, () -> {
      course.getHole(3);
    });
    assertEquals(h1, course.getHole(1));
    assertEquals(h2, course.getHole(2));
  }

  @Test
  public void testSetName() {
    course.setName("testname");
    assertEquals("testname", course.getName());
    course.setName("testname2");
    assertEquals("testname2", course.getName());
  }

  @Test
  public void testSetId() {
    course.setId("testId");
    assertEquals("testId", course.getId());
  }

  @Test
  public void testEquals() {
    course.setId("testId");
    Course course1 = new Course("testId1", "Test", Arrays.asList(new Hole(3, 45)));
    Course course2 = new Course("testId", "Test", Arrays.asList(new Hole(4, 123)));
    assertFalse(course.equals(course1), "Courses should not be equal");
    assertTrue(course.equals(course2));
  }
}
