package frisbeegolf.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import frisbeegolf.json.ReadWrite;

public class ScorecardTest {
  ReadWrite readWrite = new ReadWrite();
  Course course = readWrite.readCourse("63_402217s10_440188");
  Player player = readWrite.readPlayer("roar");
  Date date = new Date();
  List<Player> players = new ArrayList<Player>();
  Scorecard scorecard = new Scorecard(player, course, date, players);

  @Test
  public void testSavedConstructor() {
    List<Integer> testStrokes = new ArrayList<>();
    for (int i = 0; i < scorecard.getCourse().getNumberOfHoles(); i++) {
      testStrokes.add(0);
    }
    Scorecard savedScorecard = new Scorecard("testId", player, course, testStrokes, players);
    assertEquals("testId", savedScorecard.getId(), "Id was not testId");
    assertEquals("Roar", scorecard.getPlayer().getName(), "Playername was not Roar");
    assertEquals(course.getNumberOfHoles(), testStrokes.size(),
        "Not the correct amount of strokes");
  }

  @Test
  public void testId() {
    String testId = player.getId() + date.getTime();
    assertEquals(testId, scorecard.getId());
    scorecard.setId("newId");
    assertEquals("newId", scorecard.getId());
  }

  @Test
  public void testSet_Get_Stroke() {
    scorecard.setStroke(3, 1);
    assertEquals(1, scorecard.getStroke(3));
  }

  @Test
  public void testGetPar() {
    scorecard.setStroke(1, 2);
    scorecard.setStroke(3, 2);
    assertEquals(-2, scorecard.getPar(), "Did not get expected number of strokes above/under par");
  }

  @Test
  public void testListOfPlayers() {
    Player player1 = new Player("Ole");
    players.add(player1);
    Scorecard testScorecard = new Scorecard(player, course, date, players);
    assertTrue(testScorecard.getPlayers().containsAll(players));
    assertEquals("Ole", testScorecard.getPlayerFromPlayers(1).getName(), "Playername was not Ole");

  }

}
