package frisbeegolf.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;

public class ReadWriteTest {

  ReadWrite rw = new ReadWrite();

  @Test
  public void testConstructor() {
    if (Files.notExists(Paths.get(System.getProperty("user.home"), "frisbeegolf"))) {
      fail("Constructor failed to create 'frisbeegolf' folder");
    }

    if (Files.notExists(Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards"))) {
      fail("Constructor failed to create 'scorecards' folder");
    }
    if (Files.notExists(Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses"))) {
      fail("Constructor failed to create 'courses' folder");
    }
    if (Files.notExists(Paths.get(System.getProperty("user.home"), "frisbeegolf", "players"))) {
      fail("Constructor failed to create 'players' folder");
    }

    List<String> courses = rw.listFiles("courses");
    assertFalse(courses.size() == 0, "There should be courses in the home directory.");
  }

  @Test
  public void testReadCourse_FileNotExists() {
    Course course = rw.readCourse("courseTestNotExist");
    assertTrue(course == null, "Course should be null");
  }

  @Test
  public void testReadWriteCourse() {
    Course course = new Course("courseTestWriteCourse", "testName",
        Arrays.asList(new Hole(3, 99.0), new Hole(2, 25.5)));
    boolean success = rw.writeCourse(course);
    assertTrue(success, "Should have written to file");
    Course course1 = rw.readCourse("courseTestWriteCourse");
    assertEquals(3, course1.getHole(1).getPar(), "Par should be 3");
  }

  @Test
  public void testReadWriteCourse_doubleWrite() {
    Course course = new Course("courseTestWriteCourseDouble", "testName",
        Arrays.asList(new Hole(3, 99.0), new Hole(2, 25.5)));
    boolean success = rw.writeCourse(course);
    assertTrue(success, "Should have written to file");

    Course course1 = rw.readCourse("courseTestWriteCourseDouble");
    assertEquals(2, course1.getNumberOfHoles(), "Number of holes should be 2");

    course.addHole(new Hole(3, 45.9));
    boolean success1 = rw.writeCourse(course);
    assertTrue(success1, "Should have written to file");

    Course course2 = rw.readCourse("courseTestWriteCourseDouble");
    assertEquals(3, course2.getNumberOfHoles(), "Number of holes should be 3");
  }

  @Test
  public void testReadWriteScorecard() {
    Player player = new Player("testPlayer");
    List<String> friends = new ArrayList<String>();
    friends.add("Ole");
    player.addFriends(friends);
    Course course = new Course("scorecardTestCourse", "testName",
        Arrays.asList(new Hole(3, 99.0), new Hole(2, 25.5), new Hole(3, 45.9)));
    Date date = new Date();
    List<Player> players = new ArrayList<Player>();
    players.add(player);
    Scorecard scorecard = new Scorecard(player, course, date, players);
    scorecard.setId("scorecardTestReadWrite");
    scorecard.setStroke(1, 3);
    scorecard.setStroke(2, 5);
    scorecard.setStroke(3, 4);
    boolean success = rw.writeScorecard(scorecard);

    assertTrue(success, "Scorecard should be written to file");

    Scorecard readScorecard = rw.readScorecard(scorecard.getId());

    assertTrue(readScorecard != null, "Scorecard should not be null");
    assertEquals(3, readScorecard.getStroke(1), "Strokes on hole 1 should be 3");
    assertEquals(5, readScorecard.getStroke(2), "Strokes on hole 2 should be 5");
    assertEquals(4, readScorecard.getStroke(3), "Strokes on hole 2 should be 4");
    assertEquals(3, scorecard.getStrokes().size(), "Scorecard should have 3 holes");
    assertEquals("testPlayer", scorecard.getPlayer().getName(), "Playername should be testPlayer");
    assertEquals(friends, scorecard.getPlayer().getFriends(),
        "Friendslist should contain one friend; 'Ole'");
  }

  /**
   * A test made to check if ScorecardDeserializer work with a player read from file (instead of
   * local)
   */
  @Test
  public void testReadPlayerFromFile_ThroughScorecard() {
    Player player = rw.readPlayer("roar");
    Course course = rw.readCourse("63_402217s10_440188");
    Date date = new Date();
    List<Player> players = new ArrayList<Player>();
    players.add(player);
    Scorecard scorecard = new Scorecard(player, course, date, players);
    scorecard.setId("scorecardTestPlayerFromFile");
    boolean success = rw.writeScorecard(scorecard);
    assertTrue(success, "Scorecard should be written to file");

    Scorecard readScorecard = rw.readScorecard("scorecardTestPlayerFromFile");
    assertNotNull(readScorecard, "Scorecard should not be null");

    assertEquals("Roar", scorecard.getPlayer().getName(), "Playername should be Roar");
    assertEquals("roar", scorecard.getPlayer().getId(), "Id should be roar");
    assertEquals(player.getFriends(), scorecard.getPlayer().getFriends(),
        "Friendslist from roar.json originally should contain two friends");
  }

  @Test
  public void testReadScorecard_fileNotExists() {
    Scorecard scorecard = rw.readScorecard("scorecardTestNotExists");
    assertTrue(scorecard == null, "scorecard should be null");
  }

  @Test
  public void testReadPlayer_FileNotExists() {
    Player player = rw.readPlayer("playerTestNotExist");
    assertTrue(player == null, "Player should be null");
  }

  @Test
  public void testWritePlayer() {
    Player player = new Player("TestPlayer", "playerTestWritePlayer");
    List<String> friends = new ArrayList<String>();
    friends.add("Ole");
    player.addFriends(friends);
    boolean success = rw.writePlayer(player);
    assertTrue(success, "Should have written to file");

    Player player1 = rw.readPlayer("playerTestWritePlayer");
    assertEquals("TestPlayer", player1.getName(), "Playername should be TestPlayer");
    assertEquals("playerTestWritePlayer", player1.getId(), "Id should be playerTestWritePlayer");
    assertEquals(friends, player1.getFriends(),
        "Friendslist read from file should be the same as friends; 'Ole'");
  }

  @Test
  public void testWritePlayer_doubleWrite() {
    Player player = new Player("TestPlayer", "playerTestWritePlayerDouble");
    List<String> friends = new ArrayList<String>();
    friends.add("Ole");
    friends.add("Gunnar");
    player.addFriends(friends);
    boolean success = rw.writePlayer(player);
    assertTrue(success, "Should have written to file");

    Player player1 = rw.readPlayer("playerTestWritePlayerDouble");
    assertEquals(2, player1.getFriends().size(), "Number of friends should be 2");

    friends.add("Berit");
    friends.add("Celine");
    player1.addFriends(friends);
    boolean success1 = rw.writePlayer(player1);
    assertTrue(success1, "Should have written to file");

    Player player2 = rw.readPlayer("playerTestWritePlayerDouble");
    assertEquals(4, player2.getFriends().size(), "Number of friends should be 4");
  }

  @Test
  public void testListFiles() {
    Course course = new Course("testListCourse", "testName", Arrays.asList(new Hole(3, 45.9)));
    Course course1 = new Course("testListCourse1", "testName", Arrays.asList(new Hole(3, 99.0)));
    rw.writeCourse(course);
    rw.writeCourse(course1);

    Collection<String> files = rw.listFiles("courses");

    assertTrue(files.contains("testListCourse"));
    assertTrue(files.contains("testListCourse1"));
    assertFalse(files.contains("testListCourse2"));
  }

  @AfterAll
  public static void deleteFilesThatWereMade() {
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses",
        "courseTestWriteCourse.json").toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses",
        "courseTestWriteCourseDouble.json").toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses", "testListCourse.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses", "testListCourse1.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "scorecardTest.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards",
        "scorecardTestPlayerFromFile.json").toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards",
        "scorecardTestReadWrite.json").toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "players",
        "playerTestWritePlayerDouble.json").toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "players",
        "playerTestWritePlayer.json").toFile().delete();
  }
}
