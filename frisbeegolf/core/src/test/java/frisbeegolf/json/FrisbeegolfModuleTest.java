package frisbeegolf.json;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import frisbeegolf.core.*;

public class FrisbeegolfModuleTest {

  /**
   * Course:
  *   {
  *     "id": "...",
  *     "name": "...",
  *     "holes" : [ { "par": int ,"length": int ,"description": " " }, ...]
  *   }.
   *
   *
   * Scorecard:
   *  {
   *    "name": name,
   *    "id": id,
   *    "friends" : [...]
   *  }.
   *
   *
   * Player:
   *  {
   *    "name": name,
   *    "id": id,
   *    "friends" : [...]
   *  }.
   */

  private static ObjectMapper mapper;

  @BeforeAll
  public static void setUp() {
    mapper = new ObjectMapper();
    mapper.registerModule(new FrisbeegolfModule());
  }

  private final static String testCourse =
      "{\"id\":\"courseId\",\"name\":\"Test course\",\"holes\":[{\"par\":4,\"length\":140.0,\"description\":\"Hole 1\"},{\"par\":4,\"length\":140.0,\"description\":\"Hole 2\"}]}";
  private final static String testScorecard =
      "{\"id\":\"scorecardTest\",\"courseId\":\"courseTest\",\"playerId\":\"local_testPlayer\",\"strokes\":[0,0,0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,0],\"players\":[\"local_testPlayer\",\"local_friend1\"]}";
  private final static String testPlayer =
      "{\"name\":\"testPlayer\",\"id\":\"local_testPlayer\",\"friends\":[\"friend1\",\"friend2\"]}";

  private List<Hole> getHoleList(int holeCount) {
    List<Hole> holes = new ArrayList<>();

    for (int i = 1; i <= holeCount; i++) {
      int par = 4;
      Double length = 140.0;
      String description = "Hole " + i;
      holes.add(new Hole(par, length, description));
    }

    return holes;
  }

  @Test
  public void testCourseSerializer() {
    Course course = new Course("courseId", "Test course", this.getHoleList(2));

    try {
      assertEquals(testCourse, mapper.writeValueAsString(course));
    } catch (JsonProcessingException w) {
      fail();
    }
  }

  @Test
  public void testScorecardSerializer() {
    Player player = new Player("testPlayer");
    List<String> friends = new ArrayList<String>();
    friends.add("friend1");
    friends.add("friend2");
    player.addFriends(friends);
    Player friend1 = new Player("friend1");
    Course course = new Course("courseTest", "Test course", this.getHoleList(18));
    Date date = new Date();
    List<Player> players = new ArrayList<Player>();
    players.add(player);
    players.add(friend1);
    Scorecard score = new Scorecard(player, course, date, players);
    score.setId("scorecardTest");

    score.setStroke(6, 2);
    score.setStroke(4, 2);

    try {
      assertEquals(testScorecard, mapper.writeValueAsString(score));
    } catch (JsonProcessingException w) {
      fail();
    }
  }

  private void checkTestCourse(Hole hole, int index) {
    assertEquals(4, hole.getPar());
    assertEquals(140.0, hole.getLength());
    assertEquals("Hole " + index, hole.getDescription());
  }

  @Test
  public void testCourseDeserializer() {
    try {
      Course courseFromFile = mapper.readValue(testCourse, Course.class);
      int index = 1;
      for (Hole hole : courseFromFile.getHoles()) {
        checkTestCourse(hole, index);
        index++;
      }
    } catch (JsonProcessingException e) {
      fail();
    }
  }

  @Test
  public void testScorecardDeserializer() {
    try {
      Scorecard scorecardFromFile = mapper.readValue(testScorecard, Scorecard.class);
      for (int i = 1; i <= scorecardFromFile.getStrokes().size(); i++) {
        if (i != 4 && i != 6) {
          assertEquals(scorecardFromFile.getStroke(i), 0);
        } else {
          assertEquals(scorecardFromFile.getStroke(i), 2);
        }
      }
      assertEquals(scorecardFromFile.getPlayer().getName(), "testPlayer",
          "Playername should be testPlayer");
    } catch (JsonProcessingException e) {
      fail();
    }
  }

  @Test
  public void testPlayerDeserializer() {
    try {
      Player playerFromFile = mapper.readValue(testPlayer, Player.class);
      assertEquals("testPlayer", playerFromFile.getName(),
          "Playername should be testPlayer was " + playerFromFile.getName());
      assertEquals("local_testPlayer", playerFromFile.getId(),
          "Playerid should be local_testPlayer");
      assertTrue(playerFromFile.getFriends().iterator().next().equals("friend1"),
          "Friend 1 in friendlist should be friend1");
    } catch (JsonProcessingException e) {
      fail();
    }
  }

  @Test
  public void testSerializerDeserializer() {

    Player player = new Player("testPlayer");
    Course course = new Course("courseId", "Test course", this.getHoleList(6));
    Date date = new Date();
    List<Player> players = new ArrayList<Player>();
    players.add(player);
    Scorecard score = new Scorecard(player, course, date, players);
    score.setStroke(6, 2);
    score.setStroke(4, 2);

    try {

      String jsonScore = mapper.writeValueAsString(score);
      String jsonCourse = mapper.writeValueAsString(course);

      Course courseFromFile = mapper.readValue(jsonCourse, Course.class);
      int index = 1;
      for (Hole hole : courseFromFile.getHoles()) {
        checkTestCourse(hole, index);
        index++;
      }

      Scorecard scorecardFromFile = mapper.readValue(jsonScore, Scorecard.class);
      for (int i = 1; i <= scorecardFromFile.getStrokes().size(); i++) {
        if (i != 4 && i != 6) {
          assertEquals(scorecardFromFile.getStroke(i), 0);
        } else {
          assertEquals(scorecardFromFile.getStroke(i), 2);
        }
      }
      assertEquals(player.getId(), scorecardFromFile.getPlayer().getId());

    } catch (JsonProcessingException e) {
      fail();
    }
  }
}
