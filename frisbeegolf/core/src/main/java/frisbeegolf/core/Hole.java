package frisbeegolf.core;

public class Hole {
  private int par;
  private double length;
  private String description = "";

  /**
   * Initializes a Hole with par and length.
   *
   * @param par    The number of throws that is needed to get par
   * @param length The length of this hole
   */
  public Hole(int par, double length) {
    checkPar(par);
    this.par = par;
    checkLength(length);
    this.length = length;
  }

  /**
   * Initializes a Hole with par, length and a description.
   *
   * @param par         The number of throws that is needed to get par
   * @param length      The length of this hole
   * @param description Description of what is special about this hole
   */
  public Hole(int par, double length, String description) {
    this(par, length);
    this.description = description;
  }

  /**
   * Par is the number of holes expected to throw.
   *
   * @return par of the hole
   */
  public int getPar() {
    return par;
  }

  /**
   * Returns the length of the hole.
   *
   * @return length in meters. Returns -1.0 if the length is not yet set
   */
  public double getLength() {
    return length;
  }

  /**
   * Gets the description of this hole.
   *
   * @return description of hole. Returns empty string if no description is given.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the par of this hole.
   *
   * @param par Value of the par of this hole
   * @throws IllegalArgumentException Par needs to be greater than 1
   */
  public void setPar(int par) {
    checkPar(par);
    this.par = par;
  }

  /**
   * Sets the length in meters of this hole.
   *
   * @param length Value of the length of this hole
   * @throws IllegalArgumentException Length needs to be greater than zero
   */
  public void setLength(double length) {
    checkLength(length);
    this.length = length;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Checking if par is less than or equal to 1.
   *
   * @param par Given par
   * @throws IllegalArgumentException If length is less than or equal to 1
   */
  private void checkPar(int par) {
    if (par <= 1) {
      throw new IllegalArgumentException("Par of hole needs to be greater than 1");
    }
  }

  /**
   * Checking if length is less than or equal to zero.
   *
   * @param length Given length
   * @throws IllegalArgumentException If length is less than or equal to zero
   */
  private void checkLength(double length) {
    if (length <= 0) {
      throw new IllegalArgumentException("Length of hole needs to be greater than zero");
    }
  }

  @Override
  public String toString() {
    return "Hole [par=" + par + ", length=" + length + ", description=" + description + "]";
  }
}
