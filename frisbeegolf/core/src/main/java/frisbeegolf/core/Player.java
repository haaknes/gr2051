package frisbeegolf.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Player {

  private String name;
  private String id;
  private Set<String> friends = new HashSet<>();

  /**
   * Initializes a player with a name.
   *
   * @param name the name of the player
   */
  public Player(String name) {
    this.name = name;
    this.id = "local_" + name;
  }

  /**
   * Initializes a player with a name and id.
   *
   * @param name the name of the player
   * @param id   the id of the player
   */
  public Player(String name, String id) {
    this.name = name;
    this.id = id;
  }

  /**
   * Initializes a player with a name and id and friends list.
   *
   * @param name    the name of the player
   * @param id      the id of the player
   * @param friends the friends list of the player
   */
  public Player(String name, String id, Collection<String> friends) {
    this(name, id);
    this.friends.addAll(friends);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void addFriends(Collection<String> friends) {
    this.friends.addAll(friends);
  }

  public Collection<String> getFriends() {
    return new ArrayList<String>(friends);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj != null && obj instanceof Player) {
      Player player = (Player) obj;
      return this.id.equals(player.getId());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return this.id.hashCode();
  }
}
