package frisbeegolf.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Scorecard {
  private List<Integer> strokes = new ArrayList<>();
  private List<Player> playersInSameGame = new ArrayList<>();
  private String id;
  private Player player;
  private Course course;
  private int par;

  /**
   * Initializes a new scorecard with given course, player, date and time it was created and a list
   * of players that played the same game.
   *
   * @param player            the player that is going to play
   * @param course            the course that the player is going to play
   * @param time              the Date and time the scorecard was created
   * @param playersInSameGame the players that played the same game
   */
  public Scorecard(Player player, Course course, Date time, List<Player> playersInSameGame) {
    this.id = player.getId() + time.getTime();
    this.course = course;
    this.player = player;
    for (int i = 0; i < course.getNumberOfHoles(); i++) {
      strokes.add(0);
    }
    this.playersInSameGame.addAll(playersInSameGame);
  }

  /**
   * Initializes a saved scorecard with given id, player, course, strokes, and a list of players
   * that played the same game.
   *
   * @param id                Scorecard id. Should be timestamp and username.
   * @param player            the player that is going to play
   * @param course            the course that the player is going to play
   * @param strokes           how many throws are thrown on each hole
   * @param playersInSameGame the players that played the same game
   */
  public Scorecard(String id, Player player, Course course, List<Integer> strokes,
      List<Player> playersInSameGame) {
    this.id = id;
    this.player = player;
    this.course = course;
    this.strokes.addAll(strokes);
    this.playersInSameGame.addAll(playersInSameGame);
  }

  /**
   * If the stroke of a hole is more than zero, it adds the difference between strokes and the par
   * at the hole.
   *
   * @return how many strokes over/under par the player is.
   */
  public int getPar() {
    par = 0;
    for (int i = 0; i < strokes.size(); i++) {
      if (strokes.get(i) > 0) {
        par += strokes.get(i) - course.getHole(i + 1).getPar();
      }
    }
    return par;
  }

  /**
   * Returns the number of strokes of the given hole.
   *
   * @param holeNum the number of the hole
   * @return the number of stroke
   */
  public int getStroke(int holeNum) {
    return strokes.get(holeNum - 1);
  }

  /**
   * Sets the number of strokes to the given hole.
   *
   * @param holeNum the number of the hole
   * @param stroke  number of strokes
   */
  public void setStroke(int holeNum, int stroke) {
    strokes.set(holeNum - 1, stroke);
  }

  /**
   * Returns a list of strokes with indexes one smaller than the hole-number. (zero-indexed)
   *
   * @return list of number of strokes
   */
  public List<Integer> getStrokes() {
    return new ArrayList<Integer>(strokes);
  }

  /**
   * Returns a list of the players with their own scorecards that played the same game.
   * (zero-indexed)
   *
   * @return list of players that played the same game
   */
  public List<Player> getPlayers() {
    return new ArrayList<Player>(playersInSameGame);
  }

  /**
   * Returns a player from the list of players in the game.
   *
   * @param playerNum the index of the relevant player
   * @return the player on the given index
   */
  public Player getPlayerFromPlayers(int playerNum) {
    return playersInSameGame.get(playerNum - 1);
  }

  /**
   * Returns the player that has the strokes and score on this scorecard.
   *
   * @return a Player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Returns the course played.
   *
   * @return a Course
   */
  public Course getCourse() {
    return course;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj != null && obj instanceof Scorecard) {
      Scorecard scorecard = (Scorecard) obj;
      return this.id.equals(scorecard.getId());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return this.id.hashCode();
  }
}
