package frisbeegolf.core;

import java.util.ArrayList;
import java.util.List;

public class Course {
  private List<Hole> holes = new ArrayList<>();
  private String name;
  private String id;

  /**
   * Initializes this course with name of the course and the given holes.
   *
   * @param id    Course id. Should be coordinates for course location
   * @param name  String with name of this course
   * @param holes List of hole-objects
   */
  public Course(String id, String name, List<Hole> holes) {
    this.id = id;
    this.name = name;
    for (Hole hole : holes) {
      this.holes.add(hole);
    }
  }

  /**
   * Initializes this course with name of the course and the given holes.
   *
   * @param id    Course id. Should be coordinates for course location
   * @param name  String with name of this course
   * @param holes One or more hole-objects
   */
  public Course(String id, String name, Hole... holes) {
    this.id = id;
    this.name = name;
    for (Hole hole : holes) {
      this.holes.add(hole);
    }
  }

  /**
   * Initializes this course with name of the course and the given holes. The id will be generated
   * from the supplied latitude and longitude coordinates.
   *
   * @param latitude  Latitude coordinates for the location of the course
   * @param longitude Longitude coordinates for the location of the course
   * @param name      String with name of this course
   * @param holes     List of hole-objects
   */
  public Course(double latitude, double longitude, String name, List<Hole> holes) {
    this.id = generateId(latitude, longitude);
    this.name = name;
    for (Hole hole : holes) {
      this.holes.add(hole);
    }
  }

  /**
   * Checks if the number of the hole exists and returns the given hole.
   *
   * @param number The number of one specific hole on this course
   * @return The hole with the given number
   * @throws IllegalArgumentException if number is zero or below
   * @throws IllegalArgumentException if number is greater than number of holes on this course
   */
  public Hole getHole(int number) {
    checkNumber(number);
    return holes.get(number - 1);
  }

  /**
   * Checks if number is greater than number of holes or less or equal to zero.
   *
   * @param number The number of one specific hole on this course
   * @throws IllegalArgumentException if number is zero or below
   * @throws IllegalArgumentException if number is greater than number of holes on this course
   */
  private void checkNumber(int number) {
    if (number > holes.size()) {
      throw new IllegalArgumentException("Exceding number of holes on this course");
    } else if (number <= 0) {
      throw new IllegalArgumentException("Hole-numerations starts from 1");
    }
  }

  /**
   * Adds a hole to this course.
   *
   * @param hole Hole to be added
   */
  public void addHole(Hole hole) {
    this.holes.add(hole);
  }

  /**
   * Removes the hole with the given number, and decreases the number of all holes with number
   * greater than the given number.
   *
   * @param number Number of the hole that is to be removed
   * @throws IllegalArgumentException If the given number not exists in this course
   */
  public void removeHole(int number) {
    checkNumber(number);
    this.holes.remove(number - 1);
  }

  public List<Hole> getHoles() {
    return new ArrayList<Hole>(holes);
  }

  public int getNumberOfHoles() {
    return holes.size();
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  /**
   * Generates an id for this this COurse based on coordinates.
   *
   * @param latitude  Latitude-coordinates for this course
   * @param longitude Longitude-coordinates for this course
   * @return the generated id
   */
  private String generateId(double latitude, double longitude) {
    String latGen = String.valueOf(latitude).replace(".", "_");
    String longGen = String.valueOf(longitude).replace(".", "_");
    return latGen + "s" + longGen; // s for split
  }

  @Override
  public boolean equals(Object obj) {
    if (obj != null && obj instanceof Course) {
      Course course = (Course) obj;
      return this.id.equals(course.getId());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return this.id.hashCode();
  }

  public double getLat() {
    return Double.valueOf(id.split("s")[0].replace('_', '.'));
  }

  public double getLong() {
    return Double.valueOf(id.split("s")[1].replace('_', '.'));
  }
}
