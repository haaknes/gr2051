package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import frisbeegolf.core.Scorecard;
import java.io.IOException;

public class ScorecardSerializer extends JsonSerializer<Scorecard> {

  /**
   * format: {
   *  "id": "...",
   *  "courseId": "...",
   *  "player": "...",
   *  "strokes" : [{ ... }],
   *  "players" : [{ ... }]
   * }.
   */
  @Override
  public void serialize(Scorecard scorecard, JsonGenerator jsonGen,
      SerializerProvider serializerProvider) throws IOException {
    jsonGen.writeStartObject();

    jsonGen.writeStringField("id", scorecard.getId());
    jsonGen.writeStringField("courseId", scorecard.getCourse().getId());
    jsonGen.writeStringField("playerId", scorecard.getPlayer().getId());
    jsonGen.writeArrayFieldStart("strokes");
    for (int i = 1; i <= scorecard.getStrokes().size(); i++) {
      jsonGen.writeObject(scorecard.getStroke(i));
    }
    jsonGen.writeEndArray();
    jsonGen.writeArrayFieldStart("players");
    for (int i = 1; i <= scorecard.getPlayers().size(); i++) {
      jsonGen.writeString(scorecard.getPlayerFromPlayers(i).getId());
    }
    jsonGen.writeEndArray();
    jsonGen.writeEndObject();
  }
}
