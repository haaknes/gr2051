package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import frisbeegolf.core.Player;
import java.io.IOException;

public class PlayerSerializer extends JsonSerializer<Player> {

  /**
   * format: {
   *  "name": name,
   *  "id": id,
   *  "friends" : [...]
   * }.
   */
  @Override
  public void serialize(Player player, JsonGenerator jsonGen, SerializerProvider serializerProvider)
      throws IOException {
    jsonGen.writeStartObject();
    jsonGen.writeStringField("name", player.getName());
    jsonGen.writeStringField("id", player.getId());
    jsonGen.writeArrayFieldStart("friends");
    for (String friend : player.getFriends()) {
      jsonGen.writeObject(friend);
    }
    jsonGen.writeEndArray();
    jsonGen.writeEndObject();
  }
}
