package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import frisbeegolf.core.Player;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlayerDeserializer extends JsonDeserializer<Player> {
  Player player;

  /**
   * format: {
   *  "name": name,
   *  "id": id,
   *  "friends" : [...]
   * }.
   */
  @Override
  public Player deserialize(JsonParser parser, DeserializationContext deserialize)
      throws IOException, JsonProcessingException {
    JsonNode jsonNode = parser.getCodec().readTree(parser);
    return deserialize(jsonNode);
  }

  /**
   * format: {
   *  "name": name,
   *  "id": id,
   *  "friends" : [...]
   * }.
   */
  public Player deserialize(final JsonNode jsonNode) throws JsonProcessingException {
    if (jsonNode instanceof ObjectNode) {
      ObjectNode objNode = (ObjectNode) jsonNode;
      JsonNode nameNode = objNode.get("name");
      if (nameNode instanceof TextNode) {
        JsonNode idNode = objNode.get("id");
        if (idNode instanceof TextNode) {
          List<String> friends = new ArrayList<>();

          JsonNode playerNode = objNode.get("friends");
          if (playerNode instanceof ArrayNode) {
            for (JsonNode friendsNode : (ArrayNode) playerNode) {
              if (friendsNode instanceof TextNode) {
                friends.add(((TextNode) friendsNode).asText());
              }
            }
          }
          player =
              new Player(((TextNode) nameNode).asText(), ((TextNode) idNode).asText(), friends);
        }
      }

      return player;

    }

    return null;

  }
}
