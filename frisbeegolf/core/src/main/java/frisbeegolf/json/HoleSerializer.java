package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import frisbeegolf.core.Hole;
import java.io.IOException;

public class HoleSerializer extends JsonSerializer<Hole> {

  /**
   * format: { "par": int ,"length": int ,"description": " " }.
   */
  @Override
  public void serialize(Hole holes, JsonGenerator jsonGen, SerializerProvider serializerProvider)
      throws IOException {
    jsonGen.writeStartObject();
    jsonGen.writeNumberField("par", holes.getPar());
    jsonGen.writeNumberField("length", holes.getLength());
    jsonGen.writeStringField("description", holes.getDescription());
    jsonGen.writeEndObject();
  }
}
