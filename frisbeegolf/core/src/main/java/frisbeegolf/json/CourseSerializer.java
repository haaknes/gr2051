package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import java.io.IOException;

public class CourseSerializer extends JsonSerializer<Course> {

  /**
   * format:
   * {
   *  "id": "...",
   *  "name": "...",
   *  "holes" : [ { "par": int ,"length": int ,"description": " " } ...]
   * }.
   */
  @Override
  public void serialize(Course course, JsonGenerator jsonGen, SerializerProvider serializerProvider)
      throws IOException {
    jsonGen.writeStartObject();
    jsonGen.writeStringField("id", course.getId());
    jsonGen.writeStringField("name", course.getName());
    jsonGen.writeArrayFieldStart("holes");
    for (Hole hole : course.getHoles()) {
      jsonGen.writeObject(hole);
    }
    jsonGen.writeEndArray();
    jsonGen.writeEndObject();
  }
}
