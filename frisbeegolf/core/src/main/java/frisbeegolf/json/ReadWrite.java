package frisbeegolf.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import frisbeegolf.core.Course;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadWrite {

  private ObjectMapper mapper;

  /**
   * Initalizes a readWrite-object. If folders the required folders doesn't exist in the in the
   * home-space, it will set it up.
   */
  public ReadWrite() {
    mapper = new ObjectMapper();
    mapper.registerModule(new FrisbeegolfModule());


    boolean success = createFolder("frisbeegolf");
    if (!success) {
      System.err.println("Failed to create 'frisbeegolf' directory");
    }

    List<String> dataFolders = Arrays.asList("courses", "scorecards", "players");
    for (String dataFolder : dataFolders) {
      boolean dataFolderSuccess = createFolder("frisbeegolf", dataFolder);
      if (!dataFolderSuccess) {
        System.err.println("Failed to create '" + dataFolder + "' directory");
      }
    }

    // Adds sample courses from resources to the server's home space
    if (Files.exists(Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses"))) {
      List<String> sampleCourses = Arrays.asList("63_402217s10_440188", "63_409929s10_469728");
      for (String courseId : sampleCourses) {
        if (Files.notExists(Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses",
            courseId + ".json"))) {
          Course course = readCourseFromResources(courseId);
          writeCourse(course);
        }
      }
    } else {
      System.err.println("Could not initialize courses in the server's home space");
    }

    // Adds sample players from resources to the server's home space
    if (Files.exists(Paths.get(System.getProperty("user.home"), "frisbeegolf", "players"))) {
      List<String> samplePlayer = Arrays.asList("roar", "per", "lise");
      for (String playerId : samplePlayer) {
        if (Files.notExists(Paths.get(System.getProperty("user.home"), "frisbeegolf", "players",
            playerId + ".json"))) {
          Player player = readPlayerFromResources(playerId);
          writePlayer(player);
        }
      }
    } else {
      System.err.println("Could not initialize players in the server's home space");
    }
  }

  /**
   * Creates a folder in the home space.
   *
   * @param folders The path for the folder to create. The last folder is the one being created.
   * @return True if the folder was successfully created or it already exists
   */
  private boolean createFolder(String... folders) {
    if (Files.notExists(Paths.get(System.getProperty("user.home"), folders))) {
      return new File(Paths.get(System.getProperty("user.home"), folders).toString()).mkdir();
    }
    return true;
  }

  /**
   * Reads the Course from file.
   *
   * @param courseId The filename for the json file
   * @return The Course read from file or an exception error
   */
  public Course readCourse(String courseId) {
    Reader reader = null;
    Course course = null;
    try {
      reader = new FileReader(
          Paths.get(System.getProperty("user.home"), "frisbeegolf", "courses", courseId + ".json")
              .toFile(),
          StandardCharsets.UTF_8);
    } catch (IOException e) {
      System.err.println("Could not find course in home-space");
    }
    try {
      if (reader != null) {
        course = mapper.readValue(reader, Course.class);
      }
    } catch (IOException e) {
      course = null;
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (IOException e) {
        System.err.println("Could not close uninitialized stream: " + e);
      }
    }
    return course;
  }

  /**
   * Writes the given Course to file, throws exception error.
   *
   * @param course The Course to write to file
   */
  public boolean writeCourse(Course course) {
    Writer writer = null;
    boolean success = false;
    try {
      writer = new FileWriter(Paths
          .get(System.getProperty("user.home"), "frisbeegolf", "courses", course.getId() + ".json")
          .toFile(), StandardCharsets.UTF_8);
      mapper.writeValue(writer, course);
      success = true;
    } catch (IOException e) {
      System.err.println("Could not write to course with id '" + course.getId() + "': " + e);
    } finally {
      try {
        if (writer != null) {
          writer.close();
        }
      } catch (IOException e) {
        System.err.println("Could not close uninitialized stream: " + e);
      }
    }
    return success;
  }

  /**
   * Reads the Scorecard from file.
   *
   * @param scorecardId The filename for the json file
   * @return The Scorecard read from file or an exception error
   */
  public Scorecard readScorecard(String scorecardId) {
    Reader reader = null;
    Scorecard scorecard = null;
    try {
      reader = new FileReader(Paths
          .get(System.getProperty("user.home"), "frisbeegolf", "scorecards", scorecardId + ".json")
          .toFile(), StandardCharsets.UTF_8);
    } catch (IOException e) {
      System.err.println("Could not find scorecard in home-space");
    }
    try {
      if (reader != null) {
        scorecard = mapper.readValue(reader, Scorecard.class);
      }
    } catch (IOException e) {
      scorecard = null;
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (IOException e) {
        System.err.println("Could not close uninitialized stream: " + e);
      }
    }
    return scorecard;
  }

  /**
   * Writes the given Score to file, throws exception error.
   *
   * @param scorecard The Score to write to file
   */
  public boolean writeScorecard(Scorecard scorecard) {
    Writer writer = null;
    boolean success = false;
    try {
      writer = new FileWriter(Paths.get(System.getProperty("user.home"), "frisbeegolf",
          "scorecards", scorecard.getId() + ".json").toFile(), StandardCharsets.UTF_8);
      mapper.writerWithDefaultPrettyPrinter().writeValue(writer, scorecard);
      success = true;
    } catch (IOException e) {
      System.err.println("Could not write to scorecard with id '" + scorecard.getId() + "': " + e);
    } finally {
      try {
        if (writer != null) {
          writer.close();
        }
      } catch (IOException e) {
        System.err.println("Could not close uninitialized stream: " + e);
      }
    }

    return success;
  }

  /**
   * Reads the Player from file.
   *
   * @param playerId The filename for the json file
   * @return The Player read from file or an exception error
   */
  public Player readPlayer(String playerId) {
    try {
      File reader =
          Paths.get(System.getProperty("user.home"), "frisbeegolf", "players", playerId + ".json")
              .toFile();
      return mapper.readValue(reader, Player.class);
    } catch (IOException e) {
      System.err.println("Could not find player with id '" + playerId + "'");
      return null;
    }
  }

  /**
   * Writes the given Player to file, throws exception error.
   *
   * @param player The Player to write to file
   */
  public boolean writePlayer(Player player) {
    Writer writer = null;
    boolean success = false;
    try {
      writer = new FileWriter(Paths
          .get(System.getProperty("user.home"), "frisbeegolf", "players", player.getId() + ".json")
          .toFile(), StandardCharsets.UTF_8);
      mapper.writerWithDefaultPrettyPrinter().writeValue(writer, player);
      success = true;
    } catch (IOException e) {
      System.err.println("Could not write to player with id '" + player.getId() + "': " + e);
    } finally {
      try {
        if (writer != null) {
          writer.close();
        }
      } catch (IOException e) {
        System.err.println("Could not close uninitialized stream: " + e);
      }
    }

    return success;
  }

  /**
   * Gets all the files on directory that has given extension.
   *
   * @param folderName Name of the folder to list files from
   * @return List of filenames without file-extension.
   */
  public List<String> listFiles(String folderName) {
    File folder =
        new File(Paths.get(System.getProperty("user.home"), "frisbeegolf", folderName).toString());
    String[] files = folder.list();
    List<String> returnFiles = new ArrayList<>();
    if (files != null) {
      for (String file : files) {
        int index = file.lastIndexOf('.');
        if (index != -1 && file.endsWith(".json")) {
          returnFiles.add(file.substring(0, index));
        }
      }
    }
    return returnFiles;
  }

  /**
   * Reads the Course from file that exists in resourses.
   *
   * @param courseId The filename for the json file
   * @return The Course read from file or an exception error
   */
  private Course readCourseFromResources(String courseId) {
    Reader reader = null;

    Course course = null;
    try {
      reader = new InputStreamReader(
          getClass().getResource("/frisbeegolf/json/courses/" + courseId + ".json").openStream(),
          StandardCharsets.UTF_8);
    } catch (IOException e) {
      System.err.println("Could not find course in resources");
    } catch (NullPointerException e) {
      System.err.println("Could not find course in resources");
    }
    try {
      if (reader != null) {
        course = mapper.readValue(reader, Course.class);
      }
    } catch (IOException e) {
      course = null;
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (IOException e) {
        System.err.println("Could not close uninitialized stream: " + e);
      }
    }
    return course;
  }

  /**
   * Reads the Player from file that exists in resourses.
   *
   * @param playerId The filename for the json file
   * @return The Player read from file or an exception error
   */
  private Player readPlayerFromResources(String playerId) {
    Reader reader = null;

    Player player = null;
    try {
      reader = new InputStreamReader(
          getClass().getResource("/frisbeegolf/json/players/" + playerId + ".json").openStream(),
          StandardCharsets.UTF_8);
    } catch (IOException e) {
      System.err.println("Could not find player in resources");
    } catch (NullPointerException e) {
      System.err.println("Could not find player in resources");
    }
    try {
      if (reader != null) {
        player = mapper.readValue(reader, Player.class);
      }
    } catch (IOException e) {
      player = null;
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (IOException e) {
        System.err.println("Could not close uninitialized stream: " + e);
      }
    }
    return player;
  }
}
