package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CourseDeserializer extends JsonDeserializer<Course> {

  // Uses the HoleDeserializer to read what the array contains and then adds the
  // holes into the new course
  private HoleDeserializer holeDeserializer = new HoleDeserializer();

  /**
   * format:
   * {
   *  "id": "...",
   *  "name": "...",
   *  "holes" : [ { "par": int ,"length": int ,"description": " " }, ...]
   * }.
   */
  @Override
  public Course deserialize(JsonParser parser, DeserializationContext deserialize)
      throws IOException, JsonProcessingException {
    TreeNode treeNode = parser.getCodec().readTree(parser);
    if (treeNode instanceof ObjectNode) {
      ObjectNode objNode = (ObjectNode) treeNode;

      List<Hole> holes = new ArrayList<>();

      JsonNode courseNode = objNode.get("holes");
      if (courseNode instanceof ArrayNode) {
        for (JsonNode holeNode : (ArrayNode) courseNode) {
          Hole hole = holeDeserializer.deserialize(holeNode);
          if (hole != null) {
            holes.add(hole);
          }
        }
      }

      return new Course(objNode.get("id").asText(), objNode.get("name").asText(), holes);
    }

    return null;
  }
}
