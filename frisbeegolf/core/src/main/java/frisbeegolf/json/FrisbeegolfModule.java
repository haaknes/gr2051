package frisbeegolf.json;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;

public class FrisbeegolfModule extends SimpleModule {
  private static final String NAME = "FrisbeegolfModule";
  private static final VersionUtil VERSION_UTIL = new VersionUtil() {
  };
  private static final long serialVersionUID = 1;

  /**
   * Personalized module to serialize and deserialize the classes.
   */
  public FrisbeegolfModule() {
    super(NAME, VERSION_UTIL.version());
    addSerializer(Hole.class, new HoleSerializer());
    addSerializer(Course.class, new CourseSerializer());
    addSerializer(Player.class, new PlayerSerializer());
    addSerializer(Scorecard.class, new ScorecardSerializer());
    addDeserializer(Hole.class, new HoleDeserializer());
    addDeserializer(Course.class, new CourseDeserializer());
    addDeserializer(Scorecard.class, new ScorecardDeserializer());
    addDeserializer(Player.class, new PlayerDeserializer());
  }
}
