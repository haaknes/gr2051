package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import frisbeegolf.core.Hole;
import java.io.IOException;

public class HoleDeserializer extends JsonDeserializer<Hole> {

  /**
   * format: { "par": int ,"length": int ,"description": " " }.
   */
  @Override
  public Hole deserialize(JsonParser parser, DeserializationContext deserialize)
      throws IOException, JsonProcessingException {
    JsonNode jsonNode = parser.getCodec().readTree(parser);
    return deserialize(jsonNode);
  }

  /**
   * format: { "par": int ,"length": int ,"description": " " }.
   */
  public Hole deserialize(final JsonNode jsonNode) throws JsonProcessingException {
    if (jsonNode instanceof ObjectNode) {
      ObjectNode objNode = (ObjectNode) jsonNode;
      Hole hole = new Hole(2, 30);
      JsonNode parNode = objNode.get("par");
      if (parNode instanceof IntNode) {
        hole.setPar(((IntNode) parNode).asInt());
      }
      JsonNode lengthNode = objNode.get("length");
      if (lengthNode instanceof DoubleNode) {
        hole.setLength(((DoubleNode) lengthNode).asDouble());
      }
      JsonNode descriptionNode = objNode.get("description");
      if (descriptionNode instanceof TextNode) {
        hole.setDescription(((TextNode) descriptionNode).asText());
      }

      return hole;

    }

    return null;

  }
}
