package frisbeegolf.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ScorecardDeserializer extends JsonDeserializer<Scorecard> {

  /**
   * format: {
   *  "id": "...",
   *  "courseId": "...",
   *  "player": "...",
   *  "strokes" : [{ ... }],
   *  "players" : [{ ... }]
   * }.
   */
  @Override
  public Scorecard deserialize(JsonParser parser, DeserializationContext deserialize)
      throws IOException, JsonProcessingException {
    JsonNode jsonNode = parser.getCodec().readTree(parser);
    return deserialize(jsonNode);
  }

  /**
   * format: {
   *  "id": "...",
   *  "courseId": "...",
   *  "player": "...",
   *  "strokes" : [{ ... }],
   *  "players" : [{ ... }]
   * }.
   */
  public Scorecard deserialize(final JsonNode jsonNode) throws JsonProcessingException {
    if (jsonNode instanceof ObjectNode) {
      ObjectNode objNode = (ObjectNode) jsonNode;

      List<Integer> strokes = new ArrayList<>();
      List<Player> players = new ArrayList<>();

      JsonNode scoreNode = objNode.get("strokes");
      if (scoreNode instanceof ArrayNode) {
        for (JsonNode scoresNode : (ArrayNode) scoreNode) {
          strokes.add(scoresNode.asInt());
        }
      }
      ReadWrite rw = new ReadWrite();
      JsonNode playersNode = objNode.get("players");
      if (playersNode instanceof ArrayNode) {
        for (JsonNode playerNode : (ArrayNode) playersNode) {
          if (playerNode instanceof TextNode) {
            String playerId = ((TextNode) playerNode).asText();
            Player player;
            if (playerId.contains("local_")
                && playerId.substring(0, playerId.indexOf("_")).equals("local")) {
              player = new Player(playerId.substring(playerId.indexOf("_") + 1));
            } else {
              player = rw.readPlayer(playerId);
            }
            players.add(player);

          }
        }

        String playerOwnerId = objNode.get("playerId").asText();
        Player player;

        if (playerOwnerId.contains("local_")
            && playerOwnerId.substring(0, playerOwnerId.indexOf("_")).equals("local")) {
          player = new Player(playerOwnerId.substring(playerOwnerId.indexOf("_") + 1));
        } else {
          player = rw.readPlayer(playerOwnerId);
        }
        return new Scorecard(objNode.get("id").asText(), player,
            rw.readCourse(objNode.get("courseId").asText()), strokes, players);
      }
    }
    return null;
  }
}
