package frisbeegolf.ui;

import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import fxmapcontrol.Location;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class NewCourseController extends AppController {

  @FXML
  private TextField nameField;
  @FXML
  private Label latLabel;
  @FXML
  private Label longLabel;
  @FXML
  private VBox holeContainer;
  @FXML
  private Label messageLabel;
  private double latitude;
  private double longitude;
  private boolean latLongSet = false;

  /**
   * Makes a new controller for making a new course with location of the new course.
   *
   * @param location the given location for the new course
   */
  public NewCourseController(Location location) {
    latLongSet = true;
    latitude = location.getLatitude();
    longitude = location.getLongitude();
  }

  /**
   * Sets the text formatters for the latitude and longitude fields so that they only support
   * numbers. Also adds a hole to the course.
   */
  @FXML
  private void initialize() {
    latLabel.setText(String.format("%.5f", latitude) + "(N)");
    longLabel.setText(String.format("%.5f", longitude) + "(E)");
    addHole();
  }

  /**
   * Goes back to the menu.
   */
  @FXML
  private void backToMenu() {
    changeScene("Menu");
  }

  /**
   * Generates a Course object based on the information written in the text fields. Saves the
   * generated Course object to file, and goes back to menu.
   */
  @FXML
  private void saveCourse() {
    if (checkForm()) {
      List<Hole> holes = new ArrayList<>();
      for (Node node : holeContainer.getChildren()) {
        if (node instanceof NewHoleComponent) {
          NewHoleComponent holeComponent = (NewHoleComponent) node;
          holes.add(holeComponent.getHole());
        }
      }

      Course course = new Course(latitude, longitude, nameField.getText(), holes);
      remoteAccess.saveCourse(course);
      changeScene("Menu");
    }
  }

  /**
   * Adds a new hole form to the list.
   */
  @FXML
  private void addHole() {
    NewHoleComponent hole = new NewHoleComponent();
    hole.setHoleNumber(holeContainer.getChildren().size() + 1);
    holeContainer.getChildren().add(hole);
  }

  /**
   * Removes a hole form from the list.
   */
  @FXML
  private void removeHole() {
    if (holeContainer.getChildren().size() > 1) {
      holeContainer.getChildren()
          .remove(holeContainer.getChildren().get(holeContainer.getChildren().size() - 1));
    }
  }

  /**
   * Checks if the form is valid.
   *
   * @return whether or not the form is valid
   */
  private boolean checkForm() {
    if (nameField.getText().isEmpty()) {
      messageLabel.setText("Banen må ha et navn");
      return false;
    }

    if (!latLongSet) {
      messageLabel.setText("Koordinatene må være satt. Trykk på kartet for å velge");
      return false;
    }
    if (holeContainer.getChildren().size() < 1) {
      messageLabel.setText("Det må være minimum ett hull på banen");
      return false;
    }

    for (Node node : holeContainer.getChildren()) {
      if (node instanceof NewHoleComponent) {
        NewHoleComponent hole = (NewHoleComponent) node;
        if (!hole.checkForm(messageLabel)) {
          return false;
        }
      }
    }

    return true;
  }

  @FXML
  private void changeLatLong() {
    changeScene("Map", new MapControllerChooseLatLong(new Location(latitude, longitude)));
  }
}
