package frisbeegolf.ui;

import fxmapcontrol.Location;
import fxmapcontrol.MapBase;
import fxmapcontrol.MapItemsControl;
import fxmapcontrol.MapNode;
import fxmapcontrol.MapProjection;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;

public class MapControllerChooseLatLong extends AbstractMapController {

  @FXML
  private MapBase mapView;
  private Location location = null;
  private MapItemsControl<MapNode> markerPotentialParent = new MapItemsControl<MapNode>();
  private EventHandler<MouseEvent> clickedMap = this::clickedMapAddMarker;
  private boolean otherClicked = false;

  public MapControllerChooseLatLong() {
  }

  /**
   * Initializes this object with a given location of the potential place of the course.
   *
   * @param location of the potiential course.
   */
  public MapControllerChooseLatLong(Location location) {
    this.location = location;
  }

  @Override
  public void clickedInfo(String id, Location location) {

    MapItemsControl<MapNode> infoParent = getInfoParent();
    try {
      if (id.substring(0, 11).equals("closeButton")) {
        infoParent.getItems().clear();
      } else if (id.substring(0, 13).equals("chooseLatLong")) {
        if (infoParent.getItems().get(0) instanceof MapCourseInfoChooseLatLong) {
          changeScene("NewCourse", new NewCourseController(location));
        }
      }
    } catch (IndexOutOfBoundsException e) {
      System.err.println("Clicked on element with strange id" + e);
    }
    clearMarker();

  }

  @Override
  protected void mouseClickedCourse(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof MapMarker) {
      MapItemsControl<MapNode> infoParent = getInfoParent();
      MapCourseInfo courseInfo =
          new MapCourseInfo(((MapMarker) mouseEvent.getSource()).getCourse(), this);
      clearMarker();
      infoParent.getItems().clear();
      infoParent.getItems().add(courseInfo);
    }
  }

  /**
   * Removes the marker and info about the potential course.
   */
  private void clearMarker() {
    getInfoParent().getItems().clear();
    markerPotentialParent.getItems().clear();
  }

  /**
   * Adds marker and button to choose coordinates when map is clicked.
   */
  private void clickedMapAddMarker(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof MapBase && !otherClicked) {
      double x = mouseEvent.getSceneX();
      double y = mouseEvent.getSceneY();
      MapProjection projection = mapView.getProjection();
      Point2D point = new Point2D(x, y);
      MapMarkerPotentialCourse markerPotentialCourse =
          new MapMarkerPotentialCourse(projection.viewportPointToLocation(point));
      markerPotentialParent.getItems().clear();
      markerPotentialParent.getItems().add(markerPotentialCourse);
      MapItemsControl<MapNode> infoParent = getInfoParent();
      MapCourseInfoChooseLatLong chooseLatLong =
          new MapCourseInfoChooseLatLong(projection.viewportPointToLocation(point), this);
      infoParent.getItems().clear();
      infoParent.getItems().add(chooseLatLong);
    } else {
      otherClicked = !otherClicked;
    }
  }

  /**
   * Changes scene to main menu.
   */
  @FXML
  private void onBack() {
    if (location == null) {
      changeScene("Menu");
    } else {
      changeScene("NewCourse", new NewCourseController(location));
    }
  }

  /**
   * Additional initialize content of this controller that is not present in the abstract
   * controller.
   */
  @Override
  protected void abstractInitialize() {
    mapView.addEventFilter(MouseEvent.MOUSE_CLICKED, clickedMap);
    mapView.getChildren().add(markerPotentialParent);
    MapItemsControl<MapNode> infoParent = getInfoParent();
    if (location != null) {
      mapView.setCenter(location);
      MapMarkerPotentialCourse markerPotentialCourse = new MapMarkerPotentialCourse(location);
      markerPotentialParent.getItems().add(markerPotentialCourse);
      MapCourseInfoChooseLatLong chooseLatLong = new MapCourseInfoChooseLatLong(location, this);
      infoParent.getItems().add(chooseLatLong);
    } else {
      infoParent.getItems().add(new MapChooseLatLongDisplay(
          mapView.getProjection().viewportPointToLocation(new Point2D(-125, -200)), this));

    }
  }

  /**
   * Zooms in on the map.
   */

  @FXML
  @Override
  public void zoomPlus() {
    mapView.setZoomLevel(mapView.getZoomLevel() + 1);
    otherClicked = true;
  }

  /**
   * Zooms out on the map.
   */

  @FXML
  @Override
  public void zoomMinus() {
    mapView.setZoomLevel(mapView.getZoomLevel() - 1);
    otherClicked = true;
  }

  @Override
  protected void handleDragged() {
    otherClicked = true;
  }
}
