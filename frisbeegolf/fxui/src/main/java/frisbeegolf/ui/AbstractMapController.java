package frisbeegolf.ui;

import frisbeegolf.core.Course;
import fxmapcontrol.Location;
import fxmapcontrol.MapBase;
import fxmapcontrol.MapItemsControl;
import fxmapcontrol.MapNode;
import fxmapcontrol.MapProjection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public abstract class AbstractMapController extends AppController {

  @FXML
  private MapBase mapView;
  @FXML
  private VBox container;

  private MapItemsControl<MapNode> markersParent;
  private MapItemsControl<MapNode> infoParent;
  private EventHandler<MouseEvent> pressedMap = this::mousePressedMap;
  private EventHandler<MouseEvent> draggedMap = this::mouseDraggedMap;
  private EventHandler<MouseEvent> clickedCourseNode = this::mouseClickedCourse;
  private Set<Course> coursesFromFile = new HashSet<>();
  private List<Course> courses;
  private Point2D start = null;

  @FXML
  private Button pluss;
  @FXML
  private Button minus;

  protected void abstractInitialize() {
  }

  /**
   * Initializes the map with center in Trondheim and adds all courses that exits to the map.
   */
  @FXML
  private void initialize() {
    mapView.setZoomLevel(10);
    markersParent = new MapItemsControl<MapNode>();
    infoParent = new MapItemsControl<MapNode>();
    mapView.getChildren().add(markersParent);
    mapView.getChildren().add(infoParent);
    mapView.setCenter(new Location(63.402217, 10.440188));
    addAllMarkers();
    mapView.addEventHandler(MouseEvent.MOUSE_PRESSED, pressedMap);
    mapView.addEventHandler(MouseEvent.MOUSE_DRAGGED, draggedMap);
    abstractInitialize();
  }

  private void mousePressedMap(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof Node) {
      start = new Point2D(mouseEvent.getSceneX(), mouseEvent.getSceneY());
      mouseEvent.consume();
    }
  }

  private void mouseDraggedMap(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof Node) {
      double dx = mouseEvent.getSceneX() - start.getX();
      double dy = mouseEvent.getSceneY() - start.getY();
      start = start.add(dx, dy);
      MapProjection projection = mapView.getProjection();
      Point2D point = projection.locationToViewportPoint(mapView.getCenter());
      Location newCenter = projection.viewportPointToLocation(point.add(-dx, -dy));
      mapView.setCenter(newCenter);
      handleDragged();
    }
  }

  protected void handleDragged() {
  }

  /**
   * Handle action when mouse clicks on a course on the map.
   */
  protected abstract void mouseClickedCourse(MouseEvent mouseEvent);

  /**
   * Zooms in on the map.
   */
  @FXML
  public void zoomPlus() {
    mapView.setZoomLevel(mapView.getZoomLevel() + 1);
  }

  /**
   * Zooms out on the map.
   */
  @FXML
  public void zoomMinus() {
    mapView.setZoomLevel(mapView.getZoomLevel() - 1);
  }

  /**
   * Add all the courses to the map as markers. They are marked as a red dot.
   */
  private void addAllMarkers() {
    coursesFromFile.addAll(remoteAccess.getCourses());
    courses = new ArrayList<Course>(coursesFromFile);
    for (Course course : courses) {
      MapMarker marker = new MapMarker(course);
      marker.addEventHandler(MouseEvent.MOUSE_CLICKED, clickedCourseNode);
      markersParent.getItems().add(marker);
    }
  }

  /**
   * Checks witch element is clicked and does what the element is supposed to to when clicked.
   *
   * @param id The id of the element that is clicked
   */
  public void clickedInfo(String id) {
    try {
      if (id.substring(0, 11).equals("closeButton")) {
        infoParent.getItems().clear();
      } else if (id.substring(0, 12).equals("chooseCourse")) {
        if (infoParent.getItems().get(0) instanceof MapCourseInfo) {
          if (infoParent.getItems().get(0) instanceof MapCourseInfo) {
            changeScene("Players",
                new PlayersController(((MapCourseInfo) infoParent.getItems().get(0)).getCourse()));
          }
        }
      }
    } catch (IndexOutOfBoundsException e) {
      System.err.println("Clicked on element with strange id" + e);
    }
  }

  /**
   * The listener for the shape that is clicked.
   *
   * @param id       of the shape
   * @param location to the shape
   */
  public abstract void clickedInfo(String id, Location location);

  /**
   * Changes scene back to Courses.
   *
   */
  protected MapItemsControl<MapNode> getInfoParent() {
    return infoParent;
  }
}
