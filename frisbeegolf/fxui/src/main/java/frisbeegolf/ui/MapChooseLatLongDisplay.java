package frisbeegolf.ui;

import fxmapcontrol.Location;
import fxmapcontrol.MapItem;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class MapChooseLatLongDisplay extends MapItem<String> {

  private AbstractMapController mapController;
  private EventHandler<MouseEvent> clicked = this::fireClicked;
  private Group group = new Group();

  /**
   * Initializes a box with text that what to do to get location to new course.
   *
   * @param location      Position of the box
   * @param mapController controller that is listening for fired event
   */
  public MapChooseLatLongDisplay(Location location, AbstractMapController mapController) {
    this.mapController = mapController;
    setLocation(location);

    Rectangle rectangle = new Rectangle(270, 80);
    rectangle.setFill(Color.DARKGRAY);

    Rectangle buttonClear = new Rectangle(10, 10);
    buttonClear.setX(255);
    buttonClear.setY(5);
    buttonClear.setFill(Color.LIGHTGREY);
    buttonClear.setId("closeButton");
    buttonClear.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Rectangle buttonClearShadow = new Rectangle(12, 12);
    buttonClearShadow.setX(254);
    buttonClearShadow.setY(4);

    Line left = new Line(256, 6, 264, 14);
    left.setId("closeButton1");
    left.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);
    Line right = new Line(264, 6, 256, 14);
    right.setId("closeButton2");
    right.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Text display = new Text("Klikk på kartet for å\nvelge koordinater til banen");
    display.setY(30);
    display.setX(10);
    display.setFont(new Font(18.0));

    group.getChildren().addAll(rectangle, buttonClearShadow, buttonClear, left, right, display);
    getChildren().add(group);
  }

  /**
   * Fire clickedInfo-function in mapcontroller.
   *
   * @param mouseEvent The mouseEvent that fired this function
   */
  private void fireClicked(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof Shape) {
      Shape element = (Shape) mouseEvent.getSource();
      mapController.clickedInfo(element.getId(), getLocation());
    }
  }
}
