package frisbeegolf.ui;

import frisbeegolf.core.Course;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.util.Collection;
import java.util.List;

public interface FrisbeegolfAccess {

  /**
   * Get a Course by id.
   *
   * @param id The id of the Course
   * @return The Course object
   */
  public Course getCourse(String id);

  /**
   * Get a list of all Courses.
   *
   * @return A list of all Course-objects
   */
  public List<Course> getCourses();

  /**
   * Saves the given course.
   *
   * @param course The course that is to be saved
   * @return True if the course was saved, false else
   */
  public boolean saveCourse(Course course);

  /**
   * Get a Scorecard by id.
   *
   * @param id The id of the Scorecard
   * @return The Scorecard object
   */
  public Scorecard getScorecard(String id);

  /**
   * Get a list of all Scorecards.
   *
   * @return A list of all Scorecard-objects
   */
  public Collection<Scorecard> getScorecards();

  /**
   * Get a list of all Scorecards.
   *
   * @param player that is owner of scorecards
   * @return A list of all Scorecard-objects of given player
   */
  public Collection<Scorecard> getScorecards(Player player);

  /**
   * Saves the given Scorecard.
   *
   * @param scorecard The Scorecard that is to be saved
   * @return True if the Scorecard was saved, false else
   */
  public boolean saveScorecard(Scorecard scorecard);

  /**
   * Get a Player by id.
   *
   * @param id The id of the Player
   * @return The Player object
   */
  public Player getPlayer(String id);

  /**
   * Get a list of all Players.
   *
   * @return A list of all Player-objects
   */
  public List<Player> getPlayers();

  /**
   * Saves the given Player.
   *
   * @param player The Player that is to be saved
   * @return True if the Player was saved, false else
   */
  public boolean savePlayer(Player player);
}
