package frisbeegolf.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

  private static String currentUser = "";

  public static String getCurrentUser() {
    return currentUser;
  }

  public static void setCurrentUser(String newUser) {
    currentUser = newUser;
  }

  @Override
  public void start(final Stage primaryStage) throws Exception {
    final Parent parent = FXMLLoader.load(App.class.getResource("Menu.fxml"));
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  public static void main(final String[] args) {
    launch(args);
  }
}
