package frisbeegolf.ui;

import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class GamesController extends AppController {

  private Set<Scorecard> scorecardFromFile = new HashSet<>();
  private List<Scorecard> allPlayersScorecards = new ArrayList<>();
  private List<Scorecard> scores;
  private Player player;

  @FXML
  VBox scoreVBox;

  /**
   * Initializes the GUI.
   *
   */
  @FXML
  private void initialize() {
    this.player = remoteAccess.getPlayer(App.getCurrentUser());
    scorecardFromFile.addAll(remoteAccess.getScorecards(player));
    showPreviousScorecards();
  }

  /**
   * Shows all previous scorecards that are saved on either home-area or in the resourses.
   */
  private void showPreviousScorecards() {
    scores = new ArrayList<Scorecard>(scorecardFromFile);
    scores.sort((o1, o2) -> o2.getId().compareTo(o1.getId()));
    scoreVBox.setSpacing(10);
    scoreVBox.setPadding(new Insets(10, 10, 10, 10));
    for (Scorecard score : scores) {
      addToFxml(score);
    }
  }

  /**
   * Writes all the holes pars to string.
   *
   * @param scorecard The scorecard that references to the course which it was played on
   * @return a string containing the number of par per hole
   */
  private String holesToString(Scorecard scorecard) {
    StringBuilder hole = new StringBuilder();
    for (int i = 1; i <= scorecard.getCourse().getNumberOfHoles(); i++) {
      hole.append(scorecard.getCourse().getHole(i).getPar());
      hole.append(" ");
    }
    return hole.toString();
  }

  /**
   * Writes all the strokes to string.
   *
   * @param scorecard The scorecard on which the strokes are contained
   * @return a string of strokes
   */
  private String strokesToString(Scorecard scorecard) {
    StringBuilder stroke = new StringBuilder();
    for (int i = 1; i <= scorecard.getCourse().getNumberOfHoles(); i++) {
      stroke.append(scorecard.getStroke(i));
      stroke.append(" ");
    }
    return stroke.toString();
  }

  /**
   * Writes all the players to string.
   *
   * @param scorecard The scorecard on which the players are contained
   * @return a string of players
   */
  private String playersToString(Scorecard scorecard) {
    StringBuilder player = new StringBuilder();
    boolean appended = false;
    Player user = scorecard.getPlayer();
    for (int i = 1; i <= scorecard.getPlayers().size(); i++) {

      if (!scorecard.getPlayerFromPlayers(i).equals(user)) {
        if (appended) {
          player.append(", ");
          player.append(scorecard.getPlayerFromPlayers(i).getName());
        } else {
          appended = true;
          player.append(scorecard.getPlayerFromPlayers(i).getName());
        }
      }
    }
    return player.toString();
  }

  /**
   * Adds one Scorecard to the VBox.
   *
   * @param scorecard that is to be added to the VBox inside the ScrollPane.
   */
  private void addToFxml(Scorecard scorecard) {
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(15, 12, 15, 12));
    vbox.setSpacing(10);
    vbox.setStyle("-fx-background-color: #6face9;");
    vbox.setId(scorecard.getId());
    Label name = new Label(scorecard.getCourse().getName());
    name.setFont(new Font(30));
    Label holes = new Label("Par:    " + holesToString(scorecard));
    holes.setFont(new Font(20));
    holes.setId("holes");
    Label score = new Label("Kast:  " + strokesToString(scorecard));
    score.setFont(new Font(20));
    score.setId("score");
    if (scorecard.getPlayers().size() > 1) {
      Label playerLabel = new Label("Spilte med: " + playersToString(scorecard));
      playerLabel.setFont(new Font(20));
      playerLabel.setId("players" + scorecard.getPlayers().size());
      vbox.getChildren().addAll(name, playerLabel, holes, score);
    } else {
      Label playerLabel = new Label("Spilte alene");
      playerLabel.setFont(new Font(25));
      playerLabel.setId("players");
      vbox.getChildren().addAll(name, playerLabel, holes, score);
    }
    vbox.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> {
      findCorrectScorecards(scorecard);
      changeScene("Scorecard", new ScorecardController(scorecard, allPlayersScorecards));
    });
    vbox.applyCss();
    vbox.layout();
    scoreVBox.getChildren().add(vbox);
  }

  /**
   * Method for finding scorecards from the list of players that played the same game. Searches
   * through scorecards with the same time of creation.
   *
   * @param scorecard the scorecard from the "main player", that has the list of other players from
   *                  the same game
   * @return a list of scorecards from the relevant players
   */
  private List<Scorecard> findCorrectScorecards(Scorecard scorecard) {
    String filter = scorecard.getId().substring(
        player.getName().indexOf(player.getName().charAt(player.getName().length() - 1)) + 1);

    for (Scorecard readScorecard : remoteAccess.getScorecards()) {
      if (readScorecard.getId().contains(filter)) {
        List<Player> playersInGame = scorecard.getPlayers().stream()
            .filter(p -> !p.getId().equals(player.getId())).collect(Collectors.toList());
        for (Player playerInGame : playersInGame) {
          if (readScorecard.getPlayer().getId().equals(playerInGame.getId())) {
            allPlayersScorecards.add(readScorecard);
          }
        }
      }
    }

    return allPlayersScorecards;
  }

  /**
   * Changes scene to main menu.
   */
  @FXML
  private void onBack() {
    changeScene("Menu");
  }
}
