package frisbeegolf.ui;

import frisbeegolf.core.Course;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class CoursesController extends AppController {
  private Set<Course> coursesFromFile = new HashSet<>();
  private List<Course> courses;

  @FXML
  VBox courseVBox;

  /**
   * Initializes the GUI.
   *
   */
  @FXML
  private void initialize() {
    coursesFromFile.addAll(remoteAccess.getCourses());

    courses = new ArrayList<Course>(coursesFromFile);
    Comparator<Course> compareByName =
        (Course o1, Course o2) -> o1.getName().compareTo(o2.getName());
    Collections.sort(courses, compareByName);
    showAllCourses();
  }

  /**
   * Shows all courses that are saved on either home-area or in the resourses.
   */
  private void showAllCourses() {
    courseVBox.setSpacing(10);
    courseVBox.setPadding(new Insets(10, 10, 10, 10));
    for (Course course : courses) {
      addToFxml(course);
    }
  }

  /**
   * Adds one course to the VBox.
   *
   * @param course that is to be added to the VBox inside the ScrollPane.
   */
  private void addToFxml(Course course) {
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(15, 12, 15, 12));
    vbox.setSpacing(10);
    vbox.setStyle("-fx-background-color: #6face9;");
    Label name = new Label(course.getName());
    name.setFont(new Font(30));
    Label holes = new Label(course.getNumberOfHoles() + " hull");
    holes.setFont(new Font(20));
    vbox.getChildren().addAll(name, holes);
    vbox.setId(course.getId());
    vbox.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> {
      changeSceneCourse(course);
    });
    vbox.applyCss();
    vbox.layout();
    courseVBox.getChildren().add(vbox);
  }

  /**
   * Changes from scene showing courses to scene showing scorecards.
   *
   * @param course The course that is clicked and is going to be displayed.
   */
  private void changeSceneCourse(Course course) {
    changeScene("Players", new PlayersController(course));
  }

  /**
   * Changes scene to main menu.
   */
  @FXML
  private void onBack() {
    changeScene("Menu");
  }

  /**
   * Changes scene to map to choose course.
   */
  @FXML
  private void changeToMap() {
    changeScene("Map", new MapControllerChooseCourse());
  }
}
