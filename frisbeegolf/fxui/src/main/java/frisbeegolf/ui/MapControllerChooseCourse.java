package frisbeegolf.ui;

import fxmapcontrol.Location;
import fxmapcontrol.MapItemsControl;
import fxmapcontrol.MapNode;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;

public class MapControllerChooseCourse extends AbstractMapController {

  /**
   * Checks witch element is clicked and does what the element is supposed to to when clicked.
   *
   * @param id The id of the element that is clicked
   */
  @Override
  public void clickedInfo(String id, Location location) {
    MapItemsControl<MapNode> infoParent = getInfoParent();
    try {
      if (id.substring(0, 11).equals("closeButton")) {
        infoParent.getItems().clear();
      } else if (id.substring(0, 12).equals("chooseCourse")) {
        if (infoParent.getItems().get(0) instanceof MapCourseInfo) {
          changeScene("Players",
              new PlayersController(((MapCourseInfo) infoParent.getItems().get(0)).getCourse()));
        }
      }
    } catch (IndexOutOfBoundsException e) {
      System.err.println("Clicked on element with strange id" + e);
    }
  }

  @Override
  protected void mouseClickedCourse(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof MapMarker) {
      MapItemsControl<MapNode> infoParent = getInfoParent();
      MapCourseInfo courseInfo =
          new MapCourseInfoChoseCourse(((MapMarker) mouseEvent.getSource()).getCourse(), this);
      infoParent.getItems().clear();
      infoParent.getItems().add(courseInfo);
    }
  }

  /**
   * Changes scene to main menu.
   */
  @FXML
  private void onBack() {
    changeScene("Courses");
  }
}
