package frisbeegolf.ui;

import javafx.fxml.FXML;

public class MenuController extends AppController {

  /**
   * Initializes a menucontroller and sets the username if it is not set.
   */
  @FXML
  public void initialize() {
    if (App.getCurrentUser().isEmpty()) {
      App.setCurrentUser("roar");
    }
  }

  @FXML
  void openScorecard() {
    changeScene("Courses");
  }

  @FXML
  void openGames() {
    changeScene("Games");
  }

  @FXML
  void newCourse() {
    changeScene("Map", new MapControllerChooseLatLong());
  }
}
