package frisbeegolf.ui;

import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class ExtraPlayersComponent extends VBox {

  private Player player;
  private Scorecard scorecard;
  private FrisbeegolfAccess remoteAccess;
  private boolean saved = false;
  private int currentHole = 1;
  public int totPar = 0;

  @FXML
  Label nameLabel;
  @FXML
  Label throwLabel;
  @FXML
  Label scoreLabel;
  @FXML
  Button removeButton;
  @FXML
  Button addButton;
  @FXML
  Label totScoreLabel;

  /**
   * Initializes a new PlayersComponent with a Player and a Scorecard.
   *
   * @param player       Player Object
   * @param scorecard    this players Scorecard
   * @param remoteAccess the inherited access to the server
   */
  public ExtraPlayersComponent(Player player, Scorecard scorecard, FrisbeegolfAccess remoteAccess) {
    this.player = player;
    this.scorecard = scorecard;
    this.remoteAccess = remoteAccess;

    FXMLLoader loader = new FXMLLoader(getClass().getResource("ExtraPlayers.fxml"));
    loader.setRoot(this);
    loader.setController(this);

    try {
      loader.load();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Prints this Players score.
   *
   */
  @FXML
  private void initialize() {
    showHoleInformation(currentHole);
  }

  /**
   * Adds 1 thorw to the player on the current hole.
   *
   */
  @FXML
  public void addThrow() {
    int x = scorecard.getStroke(currentHole);
    scorecard.setStroke(currentHole, x + 1);

    totPar = scorecard.getPar();
    showHoleInformation(currentHole);
    saveScorecard();
  }

  /**
   * Removes a throw from the player.
   */
  @FXML
  public void removeThrow() {
    int x = scorecard.getStroke(currentHole);
    if (x == 0) {
      return;
    }
    scorecard.setStroke(currentHole, x - 1);

    totPar = scorecard.getPar();
    showHoleInformation(currentHole);
    saveScorecard();
  }

  /**
   * Shows all the information about the current hole.
   *
   */
  public void showHoleInformation(int holeNr) {
    currentHole = holeNr;
    nameLabel.setText(player.getName());
    nameLabel.setId(player.getId());
    throwLabel.setText("Kast: ");
    scoreLabel.setText("" + scorecard.getStroke(currentHole));
    scoreLabel.setId(player.getId() + "stroke");
    totPar = scorecard.getPar();
    totScoreLabel.setText("" + totPar);
  }

  /**
   * Saves the scorecard to file.
   */
  public void saveScorecard() {
    saved = true;
    remoteAccess.saveScorecard(scorecard);
  }

  /**
   * Used in test to mock remoteAccess.
   *
   * @param remoteAccess RemoteAccess to set
   */
  void setRemoteAccess(FrisbeegolfAccess remoteAccess) {
    this.remoteAccess = remoteAccess;
  }

  public boolean getSaved() {
    return saved;
  }
}
