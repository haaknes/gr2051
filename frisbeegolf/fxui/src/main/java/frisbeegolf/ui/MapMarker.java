package frisbeegolf.ui;

import frisbeegolf.core.Course;
import fxmapcontrol.Location;
import fxmapcontrol.MapItem;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class MapMarker extends MapItem<Course> {
  private Course course;

  /**
   * Makes a marker with a red circle and places it where the latlong of the course-id specifies.
   *
   * @param course The course that is going to be placed on the map
   */
  public MapMarker(Course course) {

    this.course = course;
    setLocation(new Location(course.getLat(), course.getLong()));
    Circle circle = new Circle();
    circle.setRadius(7);
    circle.setFill(Color.RED);
    getChildren().add(circle);
    setId(course.getId());

  }

  public Course getCourse() {
    return course;
  }

}
