package frisbeegolf.ui;

import frisbeegolf.core.Hole;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParsePosition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.HBox;

public class NewHoleComponent extends HBox {

  @FXML
  private Label numberLabel;
  @FXML
  private TextField parField;
  @FXML
  private TextField lengthField;
  @FXML
  private TextField descriptionField;

  /**
   * Setup so that this can be used as a JavaFX component.
   */
  public NewHoleComponent() {
    FXMLLoader loader = new FXMLLoader(getClass().getResource("NewHole.fxml"));
    loader.setRoot(this);
    loader.setController(this);

    try {
      loader.load();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Sets the text formatters for the par and length fields so that they only support numbers.
   */
  @FXML
  private void initialize() {
    parField.setTextFormatter(new TextFormatter<>(c -> {
      if (c.getControlNewText().isEmpty()) {
        return c;
      }

      try {
        Integer.parseInt(c.getControlNewText());
        return c;
      } catch (NumberFormatException e) {
        return null;
      }

    }));

    lengthField.setTextFormatter(new TextFormatter<>(c -> {
      DecimalFormat format = new DecimalFormat("#");

      if (c.getControlNewText().isEmpty()) {
        return c;
      }

      ParsePosition parsePosition = new ParsePosition(0);
      Object number = format.parse(c.getControlNewText(), parsePosition);

      if (number == null || parsePosition.getIndex() < c.getControlNewText().length()) {
        return null;
      } else {
        return c;
      }
    }));
  }

  /**
   * Generates a Hole object based on the information written in the text fields. Form should always
   * be checked with checkForm() before using getHole().
   *
   * @return a Hole object.
   */
  public Hole getHole() {
    try {
      return new Hole(Integer.parseInt(parField.getText()),
          Double.parseDouble(lengthField.getText()), descriptionField.getText());
    } catch (NumberFormatException e) {
      return null;
    }
  }

  /**
   * Checks if the form is valid.
   *
   * @param messageLabel the label to write information to if a problem is found
   * @return whether or not the form is valid
   */
  public boolean checkForm(Label messageLabel) {
    if (parField.getText().isEmpty()) {
      messageLabel.setText("Hullene må ha en par-verdi");
      return false;
    } else {
      try {
        int par = Integer.parseInt(parField.getText());

        if (par < 2 || par > 10) {
          messageLabel.setText("Par-verdiene må være heltall fra 2 til 10");
          return false;
        }
      } catch (NumberFormatException e) {
        messageLabel.setText("Par-verdiene må være heltall");
        return false;
      }
    }

    if (lengthField.getText().isEmpty()) {
      messageLabel.setText("Hullene må ha en lengde");
      return false;
    } else {
      try {
        double length = Double.parseDouble(lengthField.getText());

        if (length <= 0) {
          messageLabel.setText("Lengdene må være tall over 0");
          return false;
        }
      } catch (NumberFormatException e) {
        messageLabel.setText("Lengdene må være desimaltall");
        return false;
      }
    }

    return true;
  }

  /**
   * Shows the hole number of this component.
   *
   * @param holeNumber the hole number to show
   */
  public void setHoleNumber(int holeNumber) {
    numberLabel.setText(Integer.toString(holeNumber));
  }
}
