package frisbeegolf.ui;

import frisbeegolf.core.Course;
import fxmapcontrol.Location;
import fxmapcontrol.MapItem;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class MapCourseInfo extends MapItem<Course> {

  private Course course;
  private AbstractMapController mapController;
  private EventHandler<MouseEvent> clicked = this::fireClicked;
  private Group group = new Group();

  /**
   * Makes a box with info about the given course and a "chose this course" button.
   *
   * @param course        The course that is going to be shown.
   * @param mapController The controller that is going to listen for elements clicked by this class.
   */
  public MapCourseInfo(Course course, AbstractMapController mapController) {
    this.mapController = mapController;
    this.course = course;
    setLocation(new Location(course.getLat(), course.getLong()));

    Rectangle rectangle = new Rectangle(270, 80);
    rectangle.setFill(Color.DARKGRAY);

    Rectangle buttonClear = new Rectangle(10, 10);
    buttonClear.setX(255);
    buttonClear.setY(5);
    buttonClear.setFill(Color.LIGHTGREY);
    buttonClear.setId("closeButton");
    buttonClear.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Rectangle buttonClearShadow = new Rectangle(12, 12);
    buttonClearShadow.setX(254);
    buttonClearShadow.setY(4);

    Line left = new Line(256, 6, 264, 14);
    left.setId("closeButton1");
    left.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);
    Line right = new Line(264, 6, 256, 14);
    right.setId("closeButton2");
    right.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Text courseName = new Text(course.getName());
    courseName.setY(30);
    courseName.setX(10);
    courseName.setFont(new Font(18.0));

    Text courseNumberOfHoles = new Text("Antall hull:" + course.getNumberOfHoles());
    courseNumberOfHoles.setY(70);
    courseNumberOfHoles.setX(10);
    courseNumberOfHoles.setFont(new Font(14));

    group.getChildren().addAll(rectangle, buttonClearShadow, buttonClear, left, right, courseName,
        courseNumberOfHoles);
    getChildren().add(group);
  }

  public Course getCourse() {
    return course;
  }

  /**
   * Fire clickedInfo-function in mapcontroller.
   *
   * @param mouseEvent The mouseEvent that fired this function
   */
  private void fireClicked(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof Shape) {
      Shape element = (Shape) mouseEvent.getSource();
      mapController.clickedInfo(element.getId(), getLocation());
    }
  }

  protected Group getGroup() {
    return group;
  }

  protected EventHandler<MouseEvent> getClicked() {
    return clicked;
  }
}
