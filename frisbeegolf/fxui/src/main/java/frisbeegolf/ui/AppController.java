package frisbeegolf.ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AppController {

  @FXML
  private AnchorPane anchorPane;

  protected FrisbeegolfAccess remoteAccess;

  public AppController() {
    remoteAccess = new RemoteFrisbeegolfAccess();
  }

  /**
   * Changes the current scene.
   *
   * @param sceneName The scene to load
   */
  protected void changeScene(String sceneName) {
    try {
      Stage stage = (Stage) anchorPane.getScene().getWindow();
      Parent root = FXMLLoader.load(AppController.class.getResource(sceneName + ".fxml"));
      stage.setScene(new Scene(root));
    } catch (Exception e) {
      System.err.println("Could not change scene to '" + sceneName + "': " + e);
    }
  }

  /**
   * Changes the current scene, and attaches the specified controller.
   *
   * @param sceneName  The scene to load
   * @param controller A controller for the scene
   */
  protected void changeScene(String sceneName, AppController controller) {
    try {
      Stage stage = (Stage) anchorPane.getScene().getWindow();
      FXMLLoader loader = new FXMLLoader(AppController.class.getResource(sceneName + ".fxml"));

      if (controller instanceof ScorecardController) {
        ScorecardController sc = (ScorecardController) controller;
        loader.setController(sc);
      } else if (controller instanceof PlayersController) {
        PlayersController pc = (PlayersController) controller;
        loader.setController(pc);
      } else if (controller instanceof MapControllerChooseLatLong) {
        MapControllerChooseLatLong mcLatLong = (MapControllerChooseLatLong) controller;
        loader.setController(mcLatLong);
      } else if (controller instanceof MapControllerChooseCourse) {
        MapControllerChooseCourse mcCourse = (MapControllerChooseCourse) controller;
        loader.setController(mcCourse);
      } else if (controller instanceof NewCourseController) {
        NewCourseController newCourse = (NewCourseController) controller;
        loader.setController(newCourse);
      } else {
        throw new IllegalArgumentException("This controller can not be set from code.");
      }

      stage.setScene(new Scene((Parent) loader.load()));
    } catch (Exception e) {
      System.err.println("Could not change scene to '" + sceneName + "': " + e);
    }
  }

  /**
   * Used in test to mock remoteAccess.
   *
   * @param remoteAccess RemoteAccess to set
   */
  void setRemoteAccess(FrisbeegolfAccess remoteAccess) {
    this.remoteAccess = remoteAccess;
  }
}
