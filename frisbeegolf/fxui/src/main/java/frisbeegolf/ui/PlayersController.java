package frisbeegolf.ui;

import frisbeegolf.core.Course;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class PlayersController extends AppController {

  private Player player;
  private Course course;
  private Set<Player> players = new HashSet<>();

  @FXML
  private AnchorPane anchorPane;
  @FXML
  private Button addPlayer;
  @FXML
  private VBox playerVbox;
  @FXML
  private TextField playerField;
  @FXML
  private Button playGame;
  @FXML
  private ComboBox<Player> playerList;
  @FXML
  private Label messageLabel;

  /**
   * Initializes PlayersController with the Course that is to be played.
   *
   * @param course The current Course
   */
  public PlayersController(Course course) {
    this.course = course;
  }

  /**
   * Initializes PlayersController with the course that is to be played by the given players.
   *
   * @param course  The current Course
   * @param players The players that is going to play
   */
  public PlayersController(Course course, Collection<Player> players) {
    this(course);
    this.players.addAll(players);
  }

  /**
   * Creates the friends list and writes out the main player Roar.
   *
   */
  @FXML
  private void initialize() {
    this.player = remoteAccess.getPlayer(App.getCurrentUser());
    players.add(player);
    playerList.setCellFactory(p -> createListCell());
    playerList.setButtonCell(createListCell());
    for (String friend : player.getFriends()) {
      if (friend.startsWith("local_")) {
        playerList.getItems().add(new Player(friend.substring(friend.indexOf("_") + 1)));
      } else {
        Player p = remoteAccess.getPlayer(friend);
        playerList.getItems().add(p);
      }
    }
    playerVbox.setSpacing(10);
    addToFxml(player);
    for (Player gamePlayer : players) {
      if (!gamePlayer.equals(player)) {
        addToFxml(gamePlayer);
      }
    }
  }

  /**
   * Creats a new list cell with a friend.
   *
   */
  private ListCell<Player> createListCell() {
    return new ListCell<>() {
      @Override
      protected void updateItem(final Player player, final boolean empty) {
        super.updateItem(player, empty);
        setText(player != null ? player.getName() : null);
        setId(player != null ? player.getId() : null);
      }
    };
  }

  /**
   * Adds a friends from the list to the game.
   *
   */
  @FXML
  private void addSelectedPlayer() {
    if (playerList.getValue() != null && players.add(playerList.getValue())) {
      addToFxml(playerList.getValue());
    } else {
      messageLabel.setText("Spilleren allerede lagt til");
    }
  }

  /**
   * Adds one player to the VBox.
   *
   * @param player that is to be added to the VBox inside the ScrollPane.
   */
  private void addToFxml(Player player) {
    HBox hbox = new HBox();
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(15, 12, 15, 12));
    vbox.setSpacing(10);
    vbox.setStyle("-fx-background-color: #6face9;");
    Label name = new Label(player.getName());
    name.setId(player.getId());
    name.setFont(new Font(30));
    if (playerVbox.getChildren().size() >= 1) {
      Button removeButton = new Button("X");
      removeButton.setStyle("-fx-background-color: #336699;");
      removeButton.setTextFill(Color.WHITE);
      removeButton.setFont(new Font(18));
      removeButton.setPadding(new Insets(5, 7, 5, 7));
      removeButton.setOnAction(e2 -> {
        playerVbox.getChildren().remove(vbox);
        players.remove(player);
      });
      Region region = new Region();
      HBox.setHgrow(region, Priority.ALWAYS);
      hbox.getChildren().addAll(name, region, removeButton);
      hbox.setSpacing(15);
    } else {
      hbox.getChildren().addAll(name);
    }
    vbox.getChildren().addAll(hbox);
    vbox.applyCss();
    vbox.layout();
    playerVbox.getChildren().add(vbox);
    messageLabel.setText("");
  }

  /**
   * Creats and adds a new player to the game.
   *
   */
  @FXML
  public void createNewPlayer() {
    if (textFieldEmpty()) {
      String name = playerField.getText();
      name = name.substring(0, 1).toUpperCase() + name.substring(1);
      Player newPlayer = new Player(name);
      if (players.stream().noneMatch(p -> p.getName().equals(newPlayer.getName()))
          && players.add(newPlayer)) {
        addToFxml(newPlayer);
      } else {
        messageLabel.setText("Spilleren allerede lagt til");
      }
      playerField.clear();
    }
  }

  /**
   * Checks if the textfield is empty.
   *
   * @return whether or not the textfield is empty
   */
  private boolean textFieldEmpty() {
    if (playerField.getText().isEmpty()) {
      messageLabel.setText("Spilleren må ha et navn");
      return false;
    } else {
      return true;
    }
  }

  /**
   * Changes scene to scorecard with the selected players and course.
   *
   */
  @FXML
  public void playGame() {
    changeSceneToScorecard(course, player);
  }

  private void changeSceneToScorecard(Course course, Player player) {
    player.addFriends(players.stream().map(p -> p.getId()).filter(p -> !p.equals(player.getId()))
        .collect(Collectors.toList()));
    remoteAccess.savePlayer(player);
    List<Player> playersInScorecard = new ArrayList<>(players);
    Scorecard scorecard = new Scorecard(player, course, new Date(), playersInScorecard);
    changeScene("Scorecard", new ScorecardController(scorecard));
  }

  /**
   * Changes scene back to Courses.
   */
  @FXML
  private void onBack() {
    changeScene("Courses");
  }

}
