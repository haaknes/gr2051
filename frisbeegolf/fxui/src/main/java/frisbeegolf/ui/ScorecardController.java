package frisbeegolf.ui;

import frisbeegolf.core.Course;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class ScorecardController extends AppController {

  private Course course;
  private int currentHole;
  private List<Scorecard> scores = new ArrayList<>();
  private List<Player> players = new ArrayList<>();
  private Collection<ExtraPlayersComponent> comps = new ArrayList<>();
  private boolean newScorecard = false;

  @FXML
  Label currentHoleLabel;
  @FXML
  Label lengthLabel;
  @FXML
  Label parLabel;
  @FXML
  Label descriptionLabel;
  @FXML
  Button nextButton;
  @FXML
  Button previousButton;
  @FXML
  VBox extraPlayers;

  /**
   * Initializes the controller with a scorecard object.
   *
   * @param scorecard the scorecard that is to be initialized
   */
  public ScorecardController(Scorecard scorecard) {
    this.course = scorecard.getCourse();
    this.players.addAll(scorecard.getPlayers());
    this.players.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));
    newScorecard = true;
  }

  /**
   * Initializes the controller with a scorecard object.
   *
   * @param scorecard the scorecard that is to be initialized
   * @param scores    the scorecards of the extra players that gets printed
   */
  public ScorecardController(Scorecard scorecard, List<Scorecard> scores) {
    this(scorecard);
    this.scores.add(scorecard);
    this.scores.addAll(scores);
    this.scores.sort((s1, s2) -> s1.getId().compareTo(s2.getId()));
    newScorecard = false;
  }

  /**
   * Initializes the GUI.
   */
  @FXML
  private void initialize() {
    currentHole = 1;
    showHoleInformation();
    printPlayers();
  }

  /**
   * Prints a new fxml per player. Differentiates between printing new playerscorecards and reading
   * old.
   */
  private void printPlayers() {
    Date time = new Date();
    if (scores.isEmpty()) {
      for (Player player : players) {
        Scorecard scorecard = new Scorecard(player, course, time, players);
        ExtraPlayersComponent comp = new ExtraPlayersComponent(player, scorecard, remoteAccess);
        comps.add(comp);
        extraPlayers.getChildren().add(comp);
      }
    } else {
      for (Scorecard scorecard : scores) {
        ExtraPlayersComponent comp =
            new ExtraPlayersComponent(scorecard.getPlayer(), scorecard, remoteAccess);
        comps.add(comp);
        extraPlayers.getChildren().add(comp);
      }
    }
  }

  /**
   * Moves to next hole on the course when nextButton is clicked.
   */
  @FXML
  private void nextHole() {
    currentHole++;
    showHoleInformation();
  }

  /**
   * Moves to previous hole on the course when previousButton is clicked.
   */
  @FXML
  private void previousHole() {
    currentHole--;
    showHoleInformation();
  }

  /**
   * Sets the labels to display information about the current hole.
   */
  private void showHoleInformation() {
    currentHoleLabel.setText("Hull " + currentHole);
    lengthLabel.setText(course.getHole(currentHole).getLength() + " m");
    parLabel.setText("Par: " + course.getHole(currentHole).getPar());
    String description = course.getHole(currentHole).getDescription();
    if (!description.isBlank()) {
      descriptionLabel.setText("Beskrivelse: " + description);
    } else {
      descriptionLabel.setText("");
    }
    for (ExtraPlayersComponent comp : comps) {
      comp.showHoleInformation(currentHole);
    }

    if (currentHole <= 1) {
      previousButton.setDisable(true);
    } else {
      previousButton.setDisable(false);
    }

    if (currentHole >= course.getHoles().size()) {
      nextButton.setDisable(true);
    } else {
      nextButton.setDisable(false);
    }
  }

  /**
   * Saves the scorecard to file and exits to menu.
   */
  @FXML
  private void onSave() {
    boolean saved = false;
    for (ExtraPlayersComponent comp : comps) {
      if (comp.getSaved()) {
        saved = true;
        break;
      }
    }
    if (saved) {
      saveScorecard();
      changeScene("Menu");
    } else {
      if (newScorecard) {
        PlayersController controller = new PlayersController(course, players);
        changeScene("Players", controller);
      } else {
        changeScene("Games");
      }
    }

  }

  /**
   * Saves the scorecard to file.
   */
  private void saveScorecard() {
    for (ExtraPlayersComponent comp : comps) {
      comp.saveScorecard();
    }
  }
}
