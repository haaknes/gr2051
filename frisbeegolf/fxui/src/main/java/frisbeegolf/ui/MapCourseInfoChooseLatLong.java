package frisbeegolf.ui;

import fxmapcontrol.Location;
import fxmapcontrol.MapItem;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class MapCourseInfoChooseLatLong extends MapItem<Location> {

  private EventHandler<MouseEvent> clicked = this::fireClicked;
  private AbstractMapController mapController;


  /**
   * Initializes a button to chose coordinates.
   *
   * @param location      Position of the marker related to this button
   * @param mapController controller that is listening for fired event
   */
  public MapCourseInfoChooseLatLong(Location location, AbstractMapController mapController) {
    this.mapController = mapController;
    setLocation(location);
    Rectangle rectangle = new Rectangle(155, 60);
    rectangle.setFill(Color.DARKGRAY);

    Rectangle buttonClear = new Rectangle(10, 10);
    buttonClear.setX(144);
    buttonClear.setY(2);
    buttonClear.setFill(Color.LIGHTGREY);
    buttonClear.setId("closeButton");
    buttonClear.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Rectangle buttonClearShadow = new Rectangle(12, 12);
    buttonClearShadow.setX(143);
    buttonClearShadow.setY(1);

    Line left = new Line(145, 3, 153, 11);
    left.setId("closeButton1");
    left.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);
    Line right = new Line(153, 3, 145, 11);
    right.setId("closeButton2");
    right.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Rectangle button = new Rectangle(130, 40);
    button.setX(10);
    button.setY(10);
    button.setFill(Color.LIGHTGREY);
    button.setId("chooseLatLong");
    button.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Text buttonText = new Text("Velg koordinater");
    buttonText.setY(35);
    buttonText.setX(12);
    buttonText.setFont(new Font(14));
    buttonText.setId("chooseLatLong1");
    buttonText.addEventHandler(MouseEvent.MOUSE_CLICKED, clicked);

    Rectangle buttonShadow = new Rectangle(132, 42);
    buttonShadow.setX(9);
    buttonShadow.setY(9);
    Group group = new Group();
    group.getChildren().addAll(rectangle, buttonShadow, button, buttonText, buttonClearShadow,
        buttonClear, left, right);
    getChildren().add(group);
  }

  /**
   * Fire clickedInfo-function in mapcontroller.
   *
   * @param mouseEvent The mouseEvent that fired this function
   */
  private void fireClicked(MouseEvent mouseEvent) {
    if (mouseEvent.getSource() instanceof Shape) {
      Shape element = (Shape) mouseEvent.getSource();
      mapController.clickedInfo(element.getId(), getLocation());
    }
  }

}
