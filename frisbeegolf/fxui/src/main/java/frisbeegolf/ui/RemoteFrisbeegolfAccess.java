package frisbeegolf.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import frisbeegolf.core.Course;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import frisbeegolf.json.FrisbeegolfModule;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;

public class RemoteFrisbeegolfAccess implements FrisbeegolfAccess {

  private URI endpointBaseUri;
  private ObjectMapper mapper;

  /**
   * Initializes the URIs to the rest server end points.
   */
  public RemoteFrisbeegolfAccess() {
    try {
      String port = System.getProperty("frisbeegolf.port");
      if (port == null) {
        port = "8080";
      }

      this.endpointBaseUri = new URI("http://localhost:" + port + "/");
    } catch (URISyntaxException e) {
      System.err.println(e);
    }
    mapper = new ObjectMapper();
    mapper.registerModule(new FrisbeegolfModule());
  }

  private URI getUri(String path, String id) {
    return endpointBaseUri.resolve(path.concat(URLEncoder.encode(id, StandardCharsets.UTF_8)));
  }

  @Override
  public Course getCourse(String id) {
    HttpRequest request = HttpRequest.newBuilder(getUri("courses/", id))
        .header("Accept", "application/json").GET().build();
    Course course;
    try {
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      course = mapper.readValue(responseString, Course.class);
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }

    return course;
  }

  @Override
  public List<Course> getCourses() {
    HttpRequest request = HttpRequest.newBuilder(getUri("courses/", ""))
        .header("Accept", "application/json").GET().build();
    try {
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      List<Course> courses = mapper.readValue(responseString,
          mapper.getTypeFactory().constructCollectionType(List.class, Course.class));
      return courses;
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public boolean saveCourse(Course course) {
    try {
      String json = mapper.writeValueAsString(course);
      HttpRequest request = HttpRequest.newBuilder(getUri("courses/", course.getId()))
          .header("Accept", "application/json").header("Content-Type", "application/json")
          .PUT(BodyPublishers.ofString(json)).build();
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      return mapper.readValue(responseString, Boolean.class);
    } catch (IOException | InterruptedException e) {
      return false;
    }
  }

  @Override
  public Scorecard getScorecard(String id) {
    HttpRequest request = HttpRequest.newBuilder(getUri("scorecards/", id))
        .header("Accept", "application/json").GET().build();
    Scorecard scorecard;
    try {
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      scorecard = mapper.readValue(responseString, Scorecard.class);
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }

    return scorecard;
  }

  @Override
  public Collection<Scorecard> getScorecards() {
    HttpRequest request = HttpRequest.newBuilder(getUri("scorecards/", ""))
        .header("Accept", "application/json").GET().build();
    try {
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      Collection<Scorecard> scorecards = mapper.readValue(responseString,
          mapper.getTypeFactory().constructCollectionType(List.class, Scorecard.class));
      return scorecards;
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Collection<Scorecard> getScorecards(Player player) {
    HttpRequest request = HttpRequest.newBuilder(getUri("scorecards/all/", player.getId()))
        .header("Accept", "application/json").GET().build();
    try {
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      Collection<Scorecard> scorecards = mapper.readValue(responseString,
          mapper.getTypeFactory().constructCollectionType(List.class, Scorecard.class));
      return scorecards;
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public boolean saveScorecard(Scorecard scorecard) {
    try {
      String json = mapper.writeValueAsString(scorecard);
      HttpRequest request = HttpRequest.newBuilder(getUri("scorecards/", scorecard.getId()))
          .header("Accept", "application/json").header("Content-Type", "application/json")
          .PUT(BodyPublishers.ofString(json)).build();
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      return mapper.readValue(responseString, Boolean.class);
    } catch (IOException | InterruptedException e) {
      return false;
    }
  }

  @Override
  public Player getPlayer(String id) {
    HttpRequest request = HttpRequest.newBuilder(getUri("players/",id))
        .header("Accept", "application/json").GET().build();
    Player player;
    try {
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      player = mapper.readValue(responseString, Player.class);
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }

    return player;
  }

  @Override
  public List<Player> getPlayers() {
    HttpRequest request = HttpRequest.newBuilder(getUri("players/",""))
        .header("Accept", "application/json").GET().build();
    try {
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      List<Player> players = mapper.readValue(responseString,
          mapper.getTypeFactory().constructCollectionType(List.class, Player.class));
      return players;
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public boolean savePlayer(Player player) {
    try {
      String json = mapper.writeValueAsString(player);
      HttpRequest request = HttpRequest.newBuilder(getUri("players/",player.getId()))
          .header("Accept", "application/json").header("Content-Type", "application/json")
          .PUT(BodyPublishers.ofString(json)).build();
      HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      String responseString = response.body();
      return mapper.readValue(responseString, Boolean.class);
    } catch (IOException | InterruptedException e) {
      return false;
    }
  }
}
