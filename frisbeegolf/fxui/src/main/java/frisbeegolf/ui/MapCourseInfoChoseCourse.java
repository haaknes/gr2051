package frisbeegolf.ui;

import frisbeegolf.core.Course;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class MapCourseInfoChoseCourse extends MapCourseInfo {

  /**
   * Initializes a box with info and a button to chose course.
   *
   * @param course        the given course for the info
   * @param mapController controller that is listening for fired event
   */
  public MapCourseInfoChoseCourse(Course course, AbstractMapController mapController) {
    super(course, mapController);
    Rectangle button = new Rectangle(100, 30);
    button.setX(150);
    button.setY(45);
    button.setFill(Color.LIGHTGREY);
    button.setId("chooseCourse");
    button.addEventHandler(MouseEvent.MOUSE_CLICKED, getClicked());

    Text buttonText = new Text("Velg bane");
    buttonText.setY(70);
    buttonText.setX(160);
    buttonText.setFont(new Font(14));
    buttonText.setId("chooseCourse1");
    buttonText.addEventHandler(MouseEvent.MOUSE_CLICKED, getClicked());

    Rectangle buttonShadow = new Rectangle(102, 32);
    buttonShadow.setX(149);
    buttonShadow.setY(44);
    Group group = getGroup();
    group.getChildren().addAll(buttonShadow, button, buttonText);
  }

}
