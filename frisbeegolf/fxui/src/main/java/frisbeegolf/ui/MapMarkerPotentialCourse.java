package frisbeegolf.ui;

import fxmapcontrol.Location;
import fxmapcontrol.MapItem;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class MapMarkerPotentialCourse extends MapItem<Location> {

  /**
   * Makes a blue marker for the potential position of hte new course.
   *
   * @param location of the marker
   */
  public MapMarkerPotentialCourse(Location location) {
    setLocation(location);
    Circle circle = new Circle();
    circle.setRadius(7);
    circle.setFill(Color.BLUE);
    getChildren().add(circle);
    setId(String.valueOf(location.hashCode()));
  }

}
