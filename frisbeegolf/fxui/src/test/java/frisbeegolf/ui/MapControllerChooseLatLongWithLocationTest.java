package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import fxmapcontrol.MapBase;
import fxmapcontrol.MapItemsControl;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import fxmapcontrol.Location;

public class MapControllerChooseLatLongWithLocationTest extends ApplicationTest {
  private MapBase mapView;
  private FrisbeegolfAccess remoteAccess;

  /**
   * Initializes a map with a MapControllerChooseLatLong with a location. This simulates when a
   * person wants to change the coordinates of the course he/she is making.
   */
  @Override
  public void start(final Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(App.class.getResource("Map.fxml"));
    remoteAccess = new MockFrisbeegolfAccess();
    MapControllerChooseLatLong controller =
        new MapControllerChooseLatLong(new Location(63.4, 10.0));
    controller.setRemoteAccess(remoteAccess);
    loader.setController(controller);
    primaryStage.setScene(new Scene((Parent) loader.load()));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @BeforeEach
  public void mapViewInitialize() {
    mapView = lookup("#mapView").query();
  }

  @Test
  public void testMapMarkerPotentialCourseExists() {
    int exist = 0;
    for (Node node : mapView.getChildren()) {
      if (node instanceof MapItemsControl<?>) {
        MapItemsControl<?> item = (MapItemsControl<?>) node;
        if (item.getChildrenUnmodifiable().get(0) instanceof MapMarkerPotentialCourse) {
          exist++;
        }

      }
    }
    assertEquals(1, exist, "Not expected number of MapMarkerPotentialCourse");
  }

  @Test
  public void testClickOnPotentialCourse() {
    clickOn("#chooseLatLong");
    Label latLabel = (Label) lookup("#latLabel").query();
    assertEquals("63.40000(N)", latLabel.getText(), "Not expected latitude coordinates");
  }

  @Test
  public void testClickOnBackWithLocation() {
    clickOn("#clickBack");
    Label latLabel = (Label) lookup("#latLabel").query();
    assertEquals("63.40000(N)", latLabel.getText(), "Not expected latitude coordinates");
  }
}
