package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ScorecardControllerScoresConstructorTest extends ApplicationTest {

  private FrisbeegolfAccess remoteAccess;

  @Override
  public void start(Stage primaryStage) throws Exception {
    FXMLLoader loader = new FXMLLoader(getClass().getResource("Scorecard_test.fxml"));

    remoteAccess = new MockFrisbeegolfAccess();

    ScorecardController controller =
        new ScorecardController(remoteAccess.getScorecard("testPlayer100"),
            Arrays.asList(remoteAccess.getScorecard("testPlayer200"),
                remoteAccess.getScorecard("testPlayer300")));
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @Test
  public void testCorrectPlayers() {
    VBox playersBox = lookup("#extraPlayers").query();
    assertEquals(3, playersBox.getChildren().size(), "There should be three players");

    for (int i = 1; i <= 3; i++) {
      Label nameLabel = lookup("#testPlayer" + i).query();
      assertEquals("TestPlayer" + i, nameLabel.getText(), "Playername is wrong");
    }

  }

  @Test
  public void testPlayersStrokes() {
    for (int i = 1; i <= 3; i++) {
      Label strokeLabel = lookup("#testPlayer" + i + "stroke").query();
      assertEquals(2 + i, Integer.parseInt(strokeLabel.getText()), "Stroke is wrong");
    }
  }
}
