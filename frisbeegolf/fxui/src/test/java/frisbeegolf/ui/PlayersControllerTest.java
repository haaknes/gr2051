package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;
import frisbeegolf.core.Player;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class PlayersControllerTest extends ApplicationTest {

  private FrisbeegolfAccess remoteAccess;

  @Override
  public void start(final Stage primaryStage) throws Exception {
    App.setCurrentUser("testPlayer1");

    final FXMLLoader loader = new FXMLLoader(getClass().getResource("Players_test.fxml"));

    remoteAccess = new MockFrisbeegolfAccess();

    PlayersController controller =
        new PlayersController(remoteAccess.getCourse("63_402217s10_440188"));
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @Test
  public void testAddSelectPlayer() {
    clickOn("#playerList");
    clickOn("#testPlayer2");

    Label message = lookup("#messageLabel").query();
    ListCell<Player> name = lookup("#testPlayer2").query();
    assertEquals("", message.getText());
    assertEquals("TestPlayer2", name.getText());
  }

  @Test
  public void testAddPlayerTwice() {
    clickOn("#playerList");
    clickOn("#testPlayer2");

    clickOn("#playerField").write("TestPlayer2");
    WaitForAsyncUtils.waitForFxEvents();
    clickOn("#addPlayer");

    Label message = lookup("#messageLabel").query();
    assertEquals("Spilleren allerede lagt til", message.getText());
  }

  @Test
  public void testAddEmptyPlayer() {
    Label message = lookup("#messageLabel").query();
    TextField playerField = lookup("#playerField").query();
    playerField.setText("");
    clickOn("#addPlayer");

    assertEquals("Spilleren må ha et navn", message.getText());
  }

  @Test
  public void testCreateNewPlayer() {
    clickOn("#playerField").write("Karl");
    clickOn("#addPlayer");

    Label name = lookup("#local_Karl").query();
    assertEquals("Karl", name.getText());
  }

  @Test
  public void testClickOnPlayGame() {
    clickOn("#playGame");

    Label length = lookup("#lengthLabel").query();
    assertEquals("29.0 m", length.getText(), "Did not get expected length of first hole");
  }
}
