package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ExtraPlayerComponentTest extends ApplicationTest {

  private ExtraPlayersComponent controller;
  private FrisbeegolfAccess remoteAccess;

  @Override
  public void start(final Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(App.class.getResource("ExtraPlayers_test.fxml"));
    loader.setRoot(new VBox());

    remoteAccess = new MockFrisbeegolfAccess();
    controller = new ExtraPlayersComponent(remoteAccess.getPlayer("testPlayer1"),
        remoteAccess.getScorecard("testPlayer101"), remoteAccess);
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @Test
  public void testShowHoleInformation() {
    controller.showHoleInformation(1);

    Label name = lookup("#testPlayer1").query();
    Label playerThrow = lookup("#throwLabel").query();
    Label score = lookup("#testPlayer1stroke").query();
    Label totscore = lookup("#totScoreLabel").query();

    assertEquals("TestPlayer1", name.getText());
    assertEquals("Kast: ", playerThrow.getText());
    assertEquals("2", score.getText());
    assertEquals("-6", totscore.getText());
  }

  @Test
  public void testAddThrow() {
    clickOn("#addButton");
    WaitForAsyncUtils.waitForFxEvents();

    Label newScore = lookup("#testPlayer1stroke").query();
    assertEquals("3", newScore.getText(), "Does not have expected number of strokes");
  }

  @Test
  public void testRemoveThrow() {
    clickOn("#addButton");
    clickOn("#addButton");
    clickOn("#removeButton");
    WaitForAsyncUtils.waitForFxEvents();

    Label newScore = lookup("#testPlayer1stroke").query();
    assertEquals("3", newScore.getText(), "Does not have expected number of strokes");
  }

  @Test
  public void testRemoveThrow_NothingToRemove() {
    clickOn("#removeButton");
    clickOn("#removeButton");
    WaitForAsyncUtils.waitForFxEvents();

    Label newScore = lookup("#testPlayer1stroke").query();
    assertEquals("0", newScore.getText(), "Does not have expected number of strokes");
  }
}
