package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CoursesControllerTest extends ApplicationTest {

  private FrisbeegolfAccess remoteAccess;

  @Override
  public void start(final Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(getClass().getResource("Courses_test.fxml"));

    CoursesController controller = new CoursesController();
    remoteAccess = new MockFrisbeegolfAccess();
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @Test
  public void testClickOnCourse() {
    VBox coursesBox = lookup("#courseVBox").query();
    assertEquals(2, coursesBox.getChildren().size(), "Did not get expected number of courses");
  }
}
