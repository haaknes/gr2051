package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationTest;
import fxmapcontrol.MapBase;
import fxmapcontrol.MapItemsControl;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;

public class MapControllerChooseLatLongTest extends ApplicationTest {

  private MapBase mapView;
  private FrisbeegolfAccess remoteAccess;

  /**
   * Initializes a map with a MapControllerChooseLatLong without a location. This simulates how the
   * coordinates-selection happens form the map.
   */
  @Override
  public void start(final Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(App.class.getResource("Map.fxml"));

    remoteAccess = new MockFrisbeegolfAccess();

    MapControllerChooseLatLong controller = new MapControllerChooseLatLong();
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    primaryStage.setScene(new Scene((Parent) loader.load()));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @BeforeEach
  public void mapViewInitialize() {
    mapView = lookup("#mapView").query();
  }

  @Test
  public void testZoomOut() {
    assertEquals(mapView.getZoomLevel(), 10);
    clickOn("#minus");
    assertEquals(mapView.getZoomLevel(), 9);
  }

  @Test
  public void testZoomIn() {
    assertEquals(mapView.getZoomLevel(), 10);
    clickOn("#plus");
    assertEquals(mapView.getZoomLevel(), 11);
  }

  @Test
  public void testCloseDisplay() {
    clickOn("#closeButton");
    int exist = 0;
    for (Node node : mapView.getChildren()) {
      if (node instanceof MapItemsControl<?>) {
        MapItemsControl<?> item = (MapItemsControl<?>) node;
        if (item.getChildrenUnmodifiable().size() > 0) {
          if (item.getChildrenUnmodifiable().get(0) instanceof MapChooseLatLongDisplay) {
            exist++;
          }
        }
      }
    }
    assertEquals(0, exist, "Did not get expected number of MapChooseLatLongDisplay");
  }

  @Test
  public void testMapMarkerPotentialCourseExists() {
    dragMap();
    int exist = 0;
    for (Node node : mapView.getChildren()) {
      if (node instanceof MapItemsControl<?>) {
        MapItemsControl<?> item = (MapItemsControl<?>) node;
        if (item.getChildrenUnmodifiable().get(0) instanceof MapMarkerPotentialCourse) {
          exist++;
        }

      }
    }
    assertEquals(1, exist, "Not expected number of MapMarkerPotentialCourse");
  }

  @Test
  public void testClickOnCouurse() {
    dragMap();
    clickOn("#63_402217s10_440188");
    int exist = 0;
    int existPotential = 0;
    for (Node node : mapView.getChildren()) {
      if (node instanceof MapItemsControl<?>) {
        MapItemsControl<?> item = (MapItemsControl<?>) node;
        if (item.getChildrenUnmodifiable().size() > 0) {
          if (item.getChildrenUnmodifiable().get(0) instanceof MapMarkerPotentialCourse) {
            existPotential++;
          } else if (item.getChildrenUnmodifiable().get(0) instanceof MapCourseInfoChooseLatLong) {
            exist++;
          }
        }
      }
    }
    assertEquals(0, exist, "Did not get expected number of MapCourseInfoChooseLatLong");
    assertEquals(0, existPotential, "Did not get expected number of MapMarkerPotentialCourse");
  }

  @Test
  public void testMapCourseInfoChooseLatLongExists() {
    dragMap();
    int exist = 0;
    for (Node node : mapView.getChildren()) {
      if (node instanceof MapItemsControl<?>) {
        MapItemsControl<?> item = (MapItemsControl<?>) node;
        if (item.getChildrenUnmodifiable().size() > 0) {
          if (item.getChildrenUnmodifiable().get(0) instanceof MapCourseInfoChooseLatLong) {
            exist++;
          }
        }
      }
    }
    assertEquals(1, exist, "Not expected number of MapCourseInfoChooseLatLong");
  }

  @Test
  public void testMapCourseInfoChooseLatLongClose() {
    dragMap();
    clickOn("#closeButton");
    int exist = 0;
    for (Node node : mapView.getChildren()) {
      if (node instanceof MapItemsControl<?>) {
        MapItemsControl<?> item = (MapItemsControl<?>) node;
        if (item.getChildrenUnmodifiable().size() > 0) {
          if (item.getChildrenUnmodifiable().get(0) instanceof MapCourseInfoChooseLatLong) {
            exist++;
          }
        }
      }
    }
    assertEquals(0, exist, "Not expected number of MapCourseInfoChooseLatLong");
  }

  private void dragMap() {
    MapMarker marker = lookup("#63_402217s10_440188").query();
    double x = marker.getTranslateX();
    double y = marker.getTranslateY();
    FxRobot robot = new FxRobot();
    robot.drag("#63_402217s10_440188", MouseButton.PRIMARY).dropTo("#63_409929s10_469728");
    assertEquals(x + 42.0, marker.getTranslateX(), 1.0, "Did not move to right place");
    assertEquals(y - 26.0, marker.getTranslateY(), 1.0, "Did not move to right place");
    clickOn("#mapView");
  }

  @Test
  public void testOnBack() {
    clickOn("#clickBack");
    Button button = (Button) lookup("#startButton").query();
    assertEquals("Start", button.getText(), "Button did not have expected text");
  }
}
