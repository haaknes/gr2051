package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import fxmapcontrol.Location;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NewCourseControllerTest extends ApplicationTest {

  private FrisbeegolfAccess remoteAccess;

  @Override
  public void start(final Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(getClass().getResource("NewCourse_test.fxml"));

    remoteAccess = new MockFrisbeegolfAccess();

    NewCourseController controller = new NewCourseController(new Location(65.234567, 4.532643));
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @Test
  public void testClickAddHole() {
    VBox holeContainer = lookup("#holeContainer").query();
    assertEquals(1, holeContainer.getChildren().size(),
        "Did not get expected amount of holes before add");

    clickOn("#addHoleButton");

    assertEquals(2, holeContainer.getChildren().size(),
        "Did not get expected amount of holes after add");
    assertTrue(holeContainer.getChildren().get(1) instanceof NewHoleComponent,
        "A hole was not added");
  }

  @Test
  public void testClickAddRemoveHole() {
    VBox holeContainer = lookup("#holeContainer").query();
    interact(() -> {
      holeContainer.getChildren().add(new NewHoleComponent());
    });

    assertEquals(2, holeContainer.getChildren().size(),
        "Did not get expected amount of holes before remove");

    clickOn("#removeHoleButton");

    assertEquals(1, holeContainer.getChildren().size(),
        "Did not get expected amount of holes after remove");
  }

  @Test
  public void testClickRemoveHole_onlyOneHole() {
    VBox holeContainer = lookup("#holeContainer").query();
    assertEquals(1, holeContainer.getChildren().size(),
        "Did not get expected amount of holes before remove");

    clickOn("#removeHoleButton");

    assertEquals(1, holeContainer.getChildren().size(),
        "Did not get expected amount of holes after remove");
  }

  @Test
  public void testClickSaveCourse_missingCourseInformation() {
    clickOn("#saveButton");

    Label messageLabel = lookup("#messageLabel").query();
    assertFalse(messageLabel.getText().isEmpty(),
        "Should display a message when the form is invalid");
  }

  @Test
  public void testClickSaveCourse_missingHoleInformation() {
    clickOn("#nameField").write("New Course");

    clickOn("#saveButton");

    Label messageLabel = lookup("#messageLabel").query();
    assertFalse(messageLabel.getText().isEmpty(),
        "Should display a message when the form is invalid");
  }

  @Test
  public void testClickSaveCourse_validForm() {
    clickOn("#nameField").write("New Course");

    VBox holeContainer = lookup("#holeContainer").query();
    interact(() -> {
      holeContainer.getChildren().remove(0);
      holeContainer.getChildren().add(new NewHoleComponentMock());
    });

    clickOn("#saveButton");

    Button button = lookup("#startButton").query();
    assertEquals("Start", button.getText(), "Did not get expected button on main menu");

    Course remoteCourse = remoteAccess.getCourse("65_234567s4_532643");
    if (remoteCourse != null) {
      assertEquals("65_234567s4_532643", remoteCourse.getId(),
          "Generated course did not have expected id");
      assertEquals("New Course", remoteCourse.getName(),
          "Generated course did not have expected name");
      assertEquals(1, remoteCourse.getNumberOfHoles(),
          "Generated course did not have expected number of holes");
    } else {
      fail("Course was not written to file on server");
    }
  }

  @Test
  public void testClickBack() {
    clickOn("#backButton");

    Button button = lookup("#startButton").query();
    assertEquals("Start", button.getText(), "Did not get expected button on main menu");
  }

  public class NewHoleComponentMock extends NewHoleComponent {
    @Override
    public boolean checkForm(Label messageLabel) {
      return true;
    }

    @Override
    public Hole getHole() {
      return new Hole(3, 57.3, "Short");
    }
  }
}
