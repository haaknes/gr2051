package frisbeegolf.ui;

import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Arrays;

public class MockFrisbeegolfAccess implements FrisbeegolfAccess {

  private Map<String, Course> courses = new HashMap<>();
  private Map<String, Player> players = new HashMap<>();
  private Map<String, Scorecard> scorecards = new HashMap<>();

  public MockFrisbeegolfAccess() {
    courses.put("63_402217s10_440188", new Course("63_402217s10_440188", "Test Course 1",
        Arrays.asList(new Hole(3, 29), new Hole(3, 78), new Hole(4, 156), new Hole(3, 92))));
    courses.put("63_409929s10_469728", new Course("63_409929s10_469728", "Test Course 2",
        Arrays.asList(new Hole(3, 75), new Hole(4, 62), new Hole(3, 156))));
    players.put("testPlayer1",
        new Player("TestPlayer1", "testPlayer1", Arrays.asList("testPlayer2", "testPlayer3")));
    players.put("testPlayer2", new Player("TestPlayer2", "testPlayer2"));
    players.put("testPlayer3", new Player("TestPlayer3", "testPlayer3"));

    scorecards.put("testPlayer100",
        new Scorecard("testPlayer100", players.get("testPlayer1"),
            courses.get("63_402217s10_440188"), Arrays.asList(3, 3, 3, 3),
            Arrays.asList(players.get("testPlayer1"), players.get("testPlayer2"),
                players.get("testPlayer3"))));
    scorecards.put("testPlayer200",
        new Scorecard("testPlayer200", players.get("testPlayer2"),
            courses.get("63_402217s10_440188"), Arrays.asList(4, 4, 4, 4),
            Arrays.asList(players.get("testPlayer1"), players.get("testPlayer2"),
                players.get("testPlayer3"))));
    scorecards.put("testPlayer300",
        new Scorecard("testPlayer300", players.get("testPlayer3"),
            courses.get("63_402217s10_440188"), Arrays.asList(5, 5, 5, 5),
            Arrays.asList(players.get("testPlayer1"), players.get("testPlayer2"),
                players.get("testPlayer3"))));
    scorecards.put("testPlayer101", new Scorecard("testPlayer101", players.get("testPlayer1"),
        courses.get("63_409929s10_469728"), Arrays.asList(2, 1, 1), new ArrayList<>()));
  }

  public Course getCourse(String id) {
    return courses.get(id);
  }

  public List<Course> getCourses() {
    return new ArrayList<Course>(courses.values());
  }

  public boolean saveCourse(Course course) {
    courses.put(course.getId(), course);
    return true;
  }

  public Scorecard getScorecard(String id) {
    return scorecards.get(id);
  }

  public List<Scorecard> getScorecards() {
    return new ArrayList<Scorecard>(scorecards.values());
  }

  public List<Scorecard> getScorecards(Player player) {
    return scorecards.values().stream().filter(s -> s.getId().contains(player.getId())).collect(Collectors.toList());
  }

  public boolean saveScorecard(Scorecard scorecard) {
    scorecards.put(scorecard.getId(), scorecard);
    return true;
  }

  public Player getPlayer(String id) {
    return players.get(id);
  }

  public List<Player> getPlayers() {
    return new ArrayList<Player>(players.values());
  }

  public boolean savePlayer(Player player) {
    players.put(player.getId(), player);
    return true;
  }
}
