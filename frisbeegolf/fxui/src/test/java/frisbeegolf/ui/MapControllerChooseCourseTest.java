package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;
import fxmapcontrol.MapBase;
import fxmapcontrol.MapItemsControl;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;

public class MapControllerChooseCourseTest extends ApplicationTest {

  private MapBase mapView;

  private FrisbeegolfAccess remoteAccess;
  private MapControllerChooseCourse controller;

  @Override
  public void start(final Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(getClass().getResource("Map_test.fxml"));

    remoteAccess = new MockFrisbeegolfAccess();
    controller = new MapControllerChooseCourse();
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @BeforeEach
  public void mapViewInitialize() {
    mapView = lookup("#mapView").query();
  }

  @Test
  public void testZoomOut() {
    assertEquals(10, mapView.getZoomLevel());

    WaitForAsyncUtils.sleep(1, TimeUnit.SECONDS);
    clickOn("#minus");
    WaitForAsyncUtils.waitForFxEvents();

    assertEquals(9, mapView.getZoomLevel());
  }

  @Test
  public void testZoomIn() {
    assertEquals(10, mapView.getZoomLevel());

    WaitForAsyncUtils.sleep(1, TimeUnit.SECONDS);
    clickOn("#plus");
    WaitForAsyncUtils.waitForFxEvents();

    assertEquals(11, mapView.getZoomLevel());
  }

  @Test
  public void testDragMap() {

    MapMarker marker = lookup("#63_402217s10_440188").query();
    double x = marker.getTranslateX();
    double y = marker.getTranslateY();
    FxRobot robot = new FxRobot();
    robot.drag("#63_402217s10_440188", MouseButton.PRIMARY).dropTo("#63_409929s10_469728");

    assertEquals(x + 42.0, marker.getTranslateX(), 1.0, "Did not move to right place");
    assertEquals(y - 26.0, marker.getTranslateY(), 1.0, "Did not move to right place");
  }

  @Test
  public void testClickedInfoClear() {
    clickOn("#63_402217s10_440188");
    clickOn("#closeButton");


    Node node = mapView.getChildren().get(mapView.getChildren().size() - 1);
    if (node instanceof MapItemsControl<?>) {
      assertEquals(((MapItemsControl<?>) node).getChildrenUnmodifiable().size(), 0);
    } else {
      fail("Node not of type MapItemsControl");
    }
  }
}
