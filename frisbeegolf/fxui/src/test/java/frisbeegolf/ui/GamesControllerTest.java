package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GamesControllerTest extends ApplicationTest {

  private FrisbeegolfAccess remoteAccess;

  @Override
  public void start(final Stage primaryStage) throws Exception {
    App.setCurrentUser("testPlayer1");

    final FXMLLoader loader = new FXMLLoader(getClass().getResource("Games_test.fxml"));

    GamesController controller = new GamesController();
    remoteAccess = new MockFrisbeegolfAccess();
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @Test
  public void testCheckListOfScores() {
    VBox scoreVBox = lookup("#scoreVBox").query();
    assertEquals(2, scoreVBox.getChildren().size(),
        "There should be one scorecard from current user");
  }

  @Test
  public void testClickScorecardWithOnePlayer() {
    clickOn("#testPlayer101");

    VBox playersBox = lookup("#extraPlayers").query();
    assertEquals(1, playersBox.getChildren().size(), "There should be one player");
    Label length = lookup("#lengthLabel").query();
    assertEquals("75.0 m", length.getText(), "Did not get expected length of first hole");
    Label strokeLabel = lookup("#testPlayer1stroke").query();
    assertEquals(2, Integer.parseInt(strokeLabel.getText()), "Player should have two strokes");
  }

  @Test
  public void testClickScorecardWithMultiplePlayers() {
    clickOn("#testPlayer100");

    VBox playersBox = lookup("#extraPlayers").query();
    assertEquals(3, playersBox.getChildren().size(), "There should be three players");
    Label length = lookup("#lengthLabel").query();
    assertEquals("29.0 m", length.getText(), "Did not get expected length of first hole");

    for (int i = 1; i <= 3; i++) {
      Label nameLabel = lookup("#testPlayer" + i).query();
      Label strokeLabel = lookup("#testPlayer" + i + "stroke").query();
      assertEquals("TestPlayer" + i, nameLabel.getText(), "Wrong player name");
      assertEquals(2 + i, Integer.parseInt(strokeLabel.getText()), "Wrong player strokes");
    }
  }

  @Test
  public void testToStringMethods() {
    Label playersLabel = lookup("#players3").query();
    assertEquals("Spilte med: TestPlayer2, TestPlayer3", playersLabel.getText(),
        "Did not get the expected players");
    Label holes = lookup("#holes").query();
    assertEquals("Par:    3 4 3 ", holes.getText(), "Did not get the expected holes");
    Label score = lookup("#score").query();
    assertEquals("Kast:  2 1 1 ", score.getText(), "Did not get  the expected throws");
  }

  @Test
  public void testClickBack() {
    clickOn("#clickBack");
    Button button = lookup("#startButton").query();
    assertEquals("Start", button.getText(), "Did not get expected button on main menu");
  }
}
