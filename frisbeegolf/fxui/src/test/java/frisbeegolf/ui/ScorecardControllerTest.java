package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ScorecardControllerTest extends ApplicationTest {

  private FrisbeegolfAccess remoteAccess;

  @Override
  public void start(Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(getClass().getResource("Scorecard_test.fxml"));

    remoteAccess = new MockFrisbeegolfAccess();
    ScorecardController controller =
        new ScorecardController(remoteAccess.getScorecard("testPlayer100"));
    controller.setRemoteAccess(remoteAccess);

    loader.setController(controller);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();

  }

  @Test
  public void testPlayersEmptyScores() {
    VBox playersBox = lookup("#extraPlayers").query();
    assertEquals(3, playersBox.getChildren().size(), "There should be three players");

    for (int i = 1; i <= 3; i++) {
      Label scoreLabel = lookup("#testPlayer" + i + "stroke").query();
      assertEquals(0, Integer.parseInt(scoreLabel.getText()), "No player should have a score");
    }
  }

  @Test
  public void testClickOnNextHole() {
    clickOn("#nextButton");
    clickOn("#nextButton");
    Label length = lookup("#lengthLabel").query();
    assertEquals("156.0 m", length.getText(), "Did not get the length of the right hole");
  }

  @Test
  public void testClickOnPreviousHole() {
    clickOn("#previousButton");
    clickOn("#nextButton");
    clickOn("#previousButton");
    Label currentHole = lookup("#currentHoleLabel").query();
    assertEquals("Hull 1", currentHole.getText(), "Not on expected hole");
  }

  @Test
  public void testClickOnPreviousHoleTillEnd() {
    Button previousButton = lookup("#previousButton").query();
    assertTrue(previousButton.isDisabled(), "No previous hole");
  }

  @Test
  public void testClickTilEndOfCourses() {
    for (var i = 0; i < 9; i++) {
      clickOn("#nextButton");
    }
    Label currentHole = lookup("#currentHoleLabel").query();
    assertEquals("Hull 4", currentHole.getText(), "Not on expected hole");
  }

  @Test
  public void testOnBack() {
    clickOn("#addButton");
    clickOn("#clickBack");

    Button button = lookup("#startButton").query();
    assertEquals("Start", button.getText(), "Button on current scene not as expected");
  }
}
