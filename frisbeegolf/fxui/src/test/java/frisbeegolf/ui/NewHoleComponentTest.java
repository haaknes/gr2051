package frisbeegolf.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;

import frisbeegolf.core.Hole;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class NewHoleComponentTest extends ApplicationTest {

  private NewHoleComponent holeComponent;

  @Override
  public void start(final Stage primaryStage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(getClass().getResource("NewHole_test.fxml"));
    loader.setRoot(new HBox());
    holeComponent = new NewHoleComponent();
    loader.setController(holeComponent);
    final Parent parent = loader.load();
    primaryStage.setScene(new Scene(parent));
    primaryStage.setTitle("Frisbeegolf");
    primaryStage.show();
  }

  @Test
  public void testGetHole() {
    TextField parField = lookup("#parField").query();
    TextField lengthField = lookup("#lengthField").query();
    TextField descriptionField = lookup("#descriptionField").query();

    parField.setText("3");
    lengthField.setText("57.3");
    descriptionField.setText("Short");

    Hole hole = holeComponent.getHole();

    assertEquals(3, hole.getPar(), "Generated hole did not have expected par-value");
    assertEquals(57.3, hole.getLength(), "Generated hole did not have expected length");
    assertEquals("Short", hole.getDescription(),
        "Generated hole did not have expected description");
  }

  @Test
  public void testGetHole_noInput() {
    Hole hole = holeComponent.getHole();

    assertEquals(null, hole);
  }

  @Test
  public void testCheckForm_validForm() {
    TextField parField = lookup("#parField").query();
    TextField lengthField = lookup("#lengthField").query();
    TextField descriptionField = lookup("#descriptionField").query();
    Label messageLabel = new Label();

    parField.setText("3");
    lengthField.setText("57.3");
    descriptionField.setText("Short");

    boolean isValid = holeComponent.checkForm(messageLabel);

    assertEquals(true, isValid, "Form should be valid when all fields are filled in");
    assertTrue(messageLabel.getText().isEmpty(),
        "Should not be display a message when the form is valid");
  }

  @Test
  public void testCheckForm_missingPar() {
    TextField lengthField = lookup("#lengthField").query();
    TextField descriptionField = lookup("#descriptionField").query();
    Label messageLabel = new Label();

    lengthField.setText("57.3");
    descriptionField.setText("Short");

    boolean isValid = holeComponent.checkForm(messageLabel);

    assertEquals(false, isValid, "Form should be invalid when the par field is empty");
    assertFalse(messageLabel.getText().isEmpty(),
        "Should display a message when the form is invalid");
  }

  @Test
  public void testCheckForm_missingLength() {
    TextField parField = lookup("#parField").query();
    Label messageLabel = new Label();

    parField.setText("3");

    boolean isValid = holeComponent.checkForm(messageLabel);

    assertEquals(false, isValid, "Form should be invalid when the length field is empty");
    assertFalse(messageLabel.getText().isEmpty(),
        "Should display a message when the form is invalid");
  }

  @Test
  public void testSetHoleNumber() {
    interact(() -> {
      holeComponent.setHoleNumber(3);
    });

    Label numberLabel = lookup("#numberLabel").query();
    assertEquals("3", numberLabel.getText(), "Hole number was not the expected number");
  }

  @Test
  public void testWritePar() {
    clickOn("#parField").write("a3e");

    TextField parField = lookup("#parField").query();
    assertEquals("3", parField.getText(), "Par text field is not expected value");
  }

  @Test
  public void testWriteLength() {
    clickOn("#lengthField").write("g40e.o7");

    TextField lengthField = lookup("#lengthField").query();
    assertEquals("40.7", lengthField.getText(), "Length text field is not expected value");
  }
}
