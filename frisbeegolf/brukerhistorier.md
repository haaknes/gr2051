# Brukerhistorier
Her finnes alle brukerhistoriene som prosjektet inneholder.

## Føre poeng på en runde (us-1)
Som en frisbeegolfspiller ønsker jeg å holde oversikt over hvor mange kast jeg bruker på 
forskjellige baner og hull, slik at jeg slipper å glemme det underveis i runden og 
kan sammenligne resultatet med tidligere resultat. <br>

Brukeren har behov for å holde oversikt over hvor mange kast som er brukt på hvert hull. 
På den måten kan man sammenligne dagens resultat med tidligere resultat man har hatt. 
Det er også nyttig for å kunne holde oversikt og se hvor mange poeng over/under par man ender.

### Viktig å kunne se
- Når en spiller: hva som er par på dette hullet
- Når man spiller og når man er ferdig: hvordan man ligger an mtp par
- Etter man er ferdig: hvordan man gjorde det sammenlignet med tidligere

### Viktig å kunne gjøre
- Legge til kast underveis
- Lagre runden

## Utvide antall baner (us-2)
Som en bruker ønsker jeg å kunne velge hvilken bane jeg vil spille på ( fra en liste og kart), 
samt legge inn baner som ikke finnes fra før.<br>

Brukeren vil ha muligheten til å kunne velge mellom flere enn en bane. 
Ved å legge til en funksjon for at brukeren selv kan legge til baner åpnes 
mulighetene for å utvide appen. At brukeren kan velge mellom flere baner gjør appen mer anvendelig. 

### Viktig å kunne se
- Etter trykt på startknappen: en liste over alle tilgjengelige baner
- Alle tilgjengelige baner på et kart
- På siden av listen over baner: detaljer om banene.
- En side for å kunne legge til egne baner

### Viktig å kunne gjøre
- Bla gjennom tilgjengelige baner
- Se alle tilgjengelige baner på et kart
- Velge bane
- Lage egne baner

## Spille med flere spillere (us-3)
Som en bruker ønsker jeg å kunne legge andre spillere til
i dette poengkortet, samt legge til nye spillere,
enten med navn eller brukernavn <br>

Brukeren ønsker å kunne spille med flere spillere. Dette kan løses med
en oversikt over andre spillere som brukeren har spilt med før, slik at
det er enkelt å legge til flere kjente spillere. Brukeren skal også ha muligheten til å legge til nye spillere.

### Viktig å kunne se
- Etter valgt bane: en oversikt over "tidligere spilt med og venner"
- Etter valgt bane: felt for å føre inn navn/brukernavn på en ny spiller
- Etter valgt bane: en knapp for å fjerne en spiller som ikke skal spille alikevel.

### Viktig å kunne gjøre
- Legge til kjente eller nye spillere i poengkortet
- Fjerne spillere fra poengkortet
