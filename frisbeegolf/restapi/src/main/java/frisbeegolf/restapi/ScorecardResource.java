package frisbeegolf.restapi;

import frisbeegolf.core.Scorecard;
import frisbeegolf.json.ReadWrite;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScorecardResource {

  private static final Logger LOG = LoggerFactory.getLogger(ScorecardResource.class);
  private Scorecard scorecard;
  private String id;
  private ReadWrite rw = new ReadWrite();

  public ScorecardResource(String id) {
    this.id = id;
  }

  private void checkScorecard() {
    if (this.scorecard == null) {
      throw new IllegalArgumentException("Could not find Scorecard with id: " + this.id);
    }
  }

  /**
   * Gets a scorecard by id.
   *
   * @return the scorecard with the given id
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Scorecard getScorecard() {
    this.scorecard = rw.readScorecard(id);
    checkScorecard();
    LOG.debug("getScorecard({})", id);
    return this.scorecard;
  }


  /**
   * Saves a scorecard.
   *
   * @param scorecard that is to be saved
   * @return whether or not the scorecard was saved
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public boolean saveScorecard(Scorecard scorecard) {
    LOG.debug("saveScorecard({})", id);
    return rw.writeScorecard(scorecard);
  }

}
