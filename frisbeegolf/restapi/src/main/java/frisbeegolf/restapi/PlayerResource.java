package frisbeegolf.restapi;

import frisbeegolf.core.Player;
import frisbeegolf.json.ReadWrite;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayerResource {

  private static final Logger LOG = LoggerFactory.getLogger(PlayerResource.class);

  private Player player;
  private String id;
  private ReadWrite rw = new ReadWrite();

  public PlayerResource(String id) {
    this.id = id;
  }

  private void checkPlayer() {
    if (this.player == null) {
      throw new IllegalArgumentException("Could not find Player with id: " + this.id);
    }
  }

  /**
   * Gets a player by id.
   *
   * @return the player with the given id
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Player getPlayer() {
    this.player = rw.readPlayer(id);
    checkPlayer();
    LOG.debug("getPlayer({})", id);
    return this.player;
  }

  /**
   * Saves a player.
   *
   * @param player that is to be saved
   * @return whether or not the player was saved
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public boolean savePlayer(Player player) {
    LOG.debug("savePlayer({})", id);
    return rw.writePlayer(player);
  }
}
