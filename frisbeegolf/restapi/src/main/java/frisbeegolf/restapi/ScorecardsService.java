package frisbeegolf.restapi;

import frisbeegolf.core.Scorecard;
import frisbeegolf.json.ReadWrite;
import java.util.ArrayList;
import java.util.Collection;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path(ScorecardsService.SCORECARD_SERVICE_PATH)
public class ScorecardsService {

  private ReadWrite readWrite = new ReadWrite();
  public static final String SCORECARD_SERVICE_PATH = "scorecards";
  private static final Logger LOG = LoggerFactory.getLogger(ScorecardsService.class);

  /**
   * Gets all Scorecards.
   *
   * @return List of the scorecards
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Collection<Scorecard> getScorecards() {
    try {
      Collection<String> files = readWrite.listFiles("scorecards");
      Collection<Scorecard> scorecards = new ArrayList<>();
      for (String file : files) {
        scorecards.add(readWrite.readScorecard(file));
      }
      return scorecards;
    } catch (Exception e) {
      System.err.println(e);
      return null;
    }

  }

  /**
   * Gets all scorecards to the given player.
   *
   * @param playerId id of player that owns the scorecards
   * @return the list of scorecard of the given player
   */
  @Path("/all/{playerId}")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Collection<Scorecard> getScorecards(@PathParam("playerId") String playerId) {
    Collection<Scorecard> returnScorecards = new ArrayList<>();
    try {
      Collection<String> files = readWrite.listFiles("scorecards");
      Collection<Scorecard> scorecards = new ArrayList<>();
      for (String file : files) {
        scorecards.add(readWrite.readScorecard(file));
      }
      for (Scorecard scorecard : scorecards) {
        if (scorecard.getPlayer().getId().equals(playerId)) {
          returnScorecards.add(scorecard);
        }
        LOG.debug("getScorecards({})", playerId);
      }
    } catch (Exception e) {
      System.err.println(e);
    }
    return returnScorecards;
  }

  /**
   * Returns a scorecard specified by the id.
   *
   * @param id the id of the scorecard
   * @return the scorecard
   */
  @Path("/{id}")
  public ScorecardResource getScorecard(@PathParam("id") String id) {
    LOG.debug("Looking for Scorecard with id: " + id);
    return new ScorecardResource(id);
  }
}
