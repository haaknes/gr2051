package frisbeegolf.restapi;

import frisbeegolf.core.Course;
import frisbeegolf.json.ReadWrite;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path(CoursesService.COURSE_SERVICE_PATH)
public class CoursesService {

  private ReadWrite readWrite = new ReadWrite();
  public static final String COURSE_SERVICE_PATH = "courses";
  private static final Logger LOG = LoggerFactory.getLogger(CoursesService.class);

  /**
   * Gets all courses.
   *
   * @return List of the courses
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Course> getCourses() {
    try {
      List<String> files = readWrite.listFiles("courses");
      List<Course> courses = new ArrayList<>();
      for (String file : files) {
        courses.add(readWrite.readCourse(file));
      }
      return courses;
    } catch (Exception e) {
      System.err.println(e);
      return null;
    }

  }

  /**
   * Returns a course specified by the id.
   *
   * @param id the id of the course
   * @return the course
   */
  @Path("/{id}")
  public CourseResource getCourse(@PathParam("id") String id) {
    LOG.debug("Looking for Course with id: " + id);
    return new CourseResource(id);
  }
}
