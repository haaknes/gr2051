package frisbeegolf.restapi;

import frisbeegolf.core.Player;
import frisbeegolf.json.ReadWrite;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path(PlayersService.PLAYER_SERVICE_PATH)
public class PlayersService {

  private ReadWrite readWrite = new ReadWrite();
  public static final String PLAYER_SERVICE_PATH = "players";
  private static final Logger LOG = LoggerFactory.getLogger(PlayersService.class);

  /**
   * Gets all Scorecards.
   *
   * @return List of the scorecards
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Player> getPlayers() {
    try {
      List<String> files = readWrite.listFiles("players");
      List<Player> players = new ArrayList<>();
      for (String file : files) {
        players.add(readWrite.readPlayer(file));
      }
      return players;
    } catch (Exception e) {
      System.err.println(e);
      return null;
    }

  }

  /**
   * Returns a scorecard specified by the id.
   *
   * @param id the id of the scorecard
   * @return the scorecard
   */
  @Path("/{id}")
  public PlayerResource getPlayer(@PathParam("id") String id) {
    LOG.debug("Looking for Scorecard with id: " + id);
    return new PlayerResource(id);
  }
}
