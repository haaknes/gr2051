package frisbeegolf.restapi;

import frisbeegolf.core.Course;
import frisbeegolf.json.ReadWrite;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CourseResource {

  private static final Logger LOG = LoggerFactory.getLogger(CourseResource.class);
  private Course course;
  private String id;
  private ReadWrite rw = new ReadWrite();

  public CourseResource(String id) {
    this.id = id;
  }

  private void checkCourse() {
    if (this.course == null) {
      throw new IllegalArgumentException("Could not find Course with id: " + this.id);
    }
  }

  /**
   * Gets a course by id.
   *
   * @return the course with the given id
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Course getCourse() {
    this.course = rw.readCourse(id);
    checkCourse();
    LOG.debug("getCourse({})", id);
    return this.course;
  }

  /**
   * Saves a course.
   *
   * @param course that is to be saved
   * @return whether or not the course was saved
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public boolean saveCourse(Course course) {
    LOG.debug("saveCourse({})", id);
    return rw.writeCourse(course);
  }

}
