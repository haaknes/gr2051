package frisbeegolf.restserver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.grizzly.GrizzlyTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import frisbeegolf.core.Course;
import frisbeegolf.core.Player;
import frisbeegolf.core.Scorecard;
import frisbeegolf.json.ReadWrite;
import frisbeegolf.restapi.ScorecardsService;

public class ScorecardsServiceTest extends JerseyTest {

  private ReadWrite rw = new ReadWrite();

  protected boolean shouldLog() {
    return false;
  }

  @Override
  protected ResourceConfig configure() {
    final FrisbeegolfConfig config = new FrisbeegolfConfig();
    if (shouldLog()) {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      config.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL_SERVER, "WARNING");
    }

    return config;
  }

  @Override
  protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
    return new GrizzlyTestContainerFactory();
  }

  private ObjectMapper mapper;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    mapper = new FrisbeegolfModuleObjectMapperProvider().getContext(getClass());

    rw.writePlayer(new Player("TestPlayer1", "testPlayer1"));
    rw.writePlayer(new Player("TestPlayer2", "testPlayer2"));
  }

  @AfterEach
  public void tearDown() throws Exception {
    super.tearDown();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer102.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer103.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "players", "testPlayer1.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "players", "testPlayer2.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer202.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer203.json")
        .toFile().delete();
  }

  @Test
  public void testGet_scorecards() {
    rw.writeScorecard(new Scorecard("testPlayer102", new Player("TestPlayer1", "testPlayer1"),
        new Course("63_402217s10_440188", "TestCourse", new ArrayList<>()), new ArrayList<>(),
        new ArrayList<>()));
    rw.writeScorecard(new Scorecard("testPlayer103", new Player("TestPlayer1", "testPlayer1"),
        new Course("63_402217s10_440188", "TestCourse", new ArrayList<>()), new ArrayList<>(),
        new ArrayList<>()));

    Response response = target(ScorecardsService.SCORECARD_SERVICE_PATH)
        .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8").get();

    assertEquals(200, response.getStatus());

    try {
      List<Scorecard> scorecards = mapper.readValue(response.readEntity(String.class),
          mapper.getTypeFactory().constructCollectionType(List.class, Scorecard.class));

      assertTrue(scorecards.size() > 0);
      assertFalse(scorecards.stream().noneMatch(c -> c.getId().equals("testPlayer102")));
      assertFalse(scorecards.stream().noneMatch(c -> c.getId().equals("testPlayer103")));
    } catch (JsonProcessingException e) {
      fail(e.getMessage());
    }

    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer102.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer103.json")
        .toFile().delete();
  }

  @Test
  public void testGet_scorecardsPlayer() {
    rw.writeScorecard(new Scorecard("testPlayer202", new Player("TestPlayer1", "testPlayer1"),
        new Course("63_402217s10_440188", "TestCourse"), new ArrayList<>(), new ArrayList<>()));
    rw.writeScorecard(new Scorecard("testPlayer203", new Player("TestPlayer2", "testPlayer2"),
        new Course("63_402217s10_440188", "TestCourse"), new ArrayList<>(), new ArrayList<>()));

    Response response = target(ScorecardsService.SCORECARD_SERVICE_PATH).path("all/testPlayer1")
        .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8").get();

    assertEquals(200, response.getStatus());

    try {
      List<Scorecard> scorecards = mapper.readValue(response.readEntity(String.class),
          mapper.getTypeFactory().constructCollectionType(List.class, Scorecard.class));
      assertTrue(scorecards.size() > 0);
      assertFalse(scorecards.stream().noneMatch(c -> c.getId().equals("testPlayer202")));
      assertTrue(scorecards.stream().noneMatch(c -> c.getId().equals("testPlayer203")));
    } catch (JsonProcessingException e) {
      fail(e.getMessage());
    }
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer102.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer203.json")
        .toFile().delete();

  }

  @Test
  public void testGet_scorecard() {
    rw.writeScorecard(new Scorecard("testPlayer101", new Player("TestPlayer1", "testPlayer1"),
        new Course("63_402217s10_440188", "TestCourse", new ArrayList<>()), new ArrayList<>(),
        new ArrayList<>()));

    Response response = target(ScorecardsService.SCORECARD_SERVICE_PATH).path("testPlayer101")
        .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8").get();

    assertEquals(200, response.getStatus());
    try {
      Scorecard scorecard = mapper.readValue(response.readEntity(String.class), Scorecard.class);
      assertEquals("testPlayer101", scorecard.getId());
      assertEquals("testPlayer1", scorecard.getPlayer().getId());
      assertEquals("63_402217s10_440188", scorecard.getCourse().getId());
    } catch (JsonProcessingException e) {
      fail(e.getMessage());
    }

    Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards", "testPlayer101.json")
        .toFile().delete();
  }

  @Test
  public void testPut_scorecard() {
    Scorecard savedScorecard =
        new Scorecard(rw.readPlayer("roar"), rw.readCourse("63_402217s10_440188"), new Date(),
            Arrays.asList(rw.readPlayer("roar"), rw.readPlayer("lise")));
    try {
      String json = mapper.writeValueAsString(savedScorecard);
      Response response =
          target(ScorecardsService.SCORECARD_SERVICE_PATH).path(savedScorecard.getId())
              .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8")
              .put(Entity.json(json));

      assertEquals(200, response.getStatus());

      Scorecard scorecard = rw.readScorecard(savedScorecard.getId());

      assertEquals(savedScorecard.getId(), scorecard.getId());
      assertEquals("roar", scorecard.getPlayer().getId());
      assertEquals("63_402217s10_440188", scorecard.getCourse().getId());

      Paths.get(System.getProperty("user.home"), "frisbeegolf", "scorecards",
          savedScorecard.getId() + ".json").toFile().delete();
    } catch (JsonProcessingException e) {
    }
  }
}
