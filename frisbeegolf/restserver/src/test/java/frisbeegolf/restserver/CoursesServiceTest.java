package frisbeegolf.restserver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.grizzly.GrizzlyTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import frisbeegolf.core.Course;
import frisbeegolf.core.Hole;
import frisbeegolf.json.ReadWrite;
import frisbeegolf.restapi.CoursesService;

public class CoursesServiceTest extends JerseyTest {

  private ReadWrite rw = new ReadWrite();

  protected boolean shouldLog() {
    return false;
  }

  @Override
  protected ResourceConfig configure() {
    final FrisbeegolfConfig config = new FrisbeegolfConfig();
    if (shouldLog()) {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      config.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL_SERVER, "WARNING");
    }

    return config;
  }

  @Override
  protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
    return new GrizzlyTestContainerFactory();
  }

  private ObjectMapper mapper;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    mapper = new FrisbeegolfModuleObjectMapperProvider().getContext(getClass());
  }

  @AfterEach
  public void tearDown() throws Exception {
    super.tearDown();
  }

  @Test
  public void testGet_courses() {
    Response response = target(CoursesService.COURSE_SERVICE_PATH)
        .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8").get();

    assertEquals(200, response.getStatus());

    try {
      List<Course> courses = mapper.readValue(response.readEntity(String.class),
          mapper.getTypeFactory().constructCollectionType(List.class, Course.class));

      assertTrue(courses.size() > 0);
      assertFalse(courses.stream().noneMatch(c -> c.getId().equals("63_409929s10_469728")));
      assertFalse(courses.stream().noneMatch(c -> c.getId().equals("63_402217s10_440188")));
    } catch (JsonProcessingException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testGet_course() {
    Response response = target(CoursesService.COURSE_SERVICE_PATH).path("63_409929s10_469728")
        .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8").get();

    assertEquals(200, response.getStatus());

    try {
      Course course = mapper.readValue(response.readEntity(String.class), Course.class);

      assertEquals("63_409929s10_469728", course.getId());
      assertEquals("Dragvoll Diskgolfpark", course.getName());
      assertEquals(18, course.getNumberOfHoles());
    } catch (JsonProcessingException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testPut_course() {
    Course savedCourse =
        new Course("89_99999s89_999999", "Test Course", Arrays.asList(new Hole(4, 156), new Hole(3, 65)));

    try {
      String json = mapper.writeValueAsString(savedCourse);
      Response response = target(CoursesService.COURSE_SERVICE_PATH).path("testCourse1")
          .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8")
          .put(Entity.json(json));

      assertEquals(200, response.getStatus());

      Course course = rw.readCourse("89_99999s89_999999");

      assertEquals("89_99999s89_999999", course.getId());
      assertEquals("Test Course", course.getName());
      assertEquals(2, course.getNumberOfHoles());

      Paths
          .get(System.getProperty("user.home"), "frisbeegolf", "courses", "89_99999s89_999999.json")
          .toFile().delete();
    } catch (JsonProcessingException e) {
    }
  }
}
