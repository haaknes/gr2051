package frisbeegolf.restserver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.nio.file.Paths;
import java.util.List;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.grizzly.GrizzlyTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import frisbeegolf.core.Player;
import frisbeegolf.json.ReadWrite;
import frisbeegolf.restapi.PlayersService;

public class PlayersServiceTest extends JerseyTest {

  private ReadWrite rw = new ReadWrite();

  protected boolean shouldLog() {
    return false;
  }

  @Override
  protected ResourceConfig configure() {
    final FrisbeegolfConfig config = new FrisbeegolfConfig();
    if (shouldLog()) {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      config.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL_SERVER, "WARNING");
    }

    return config;
  }

  @Override
  protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
    return new GrizzlyTestContainerFactory();
  }

  private ObjectMapper mapper;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    mapper = new FrisbeegolfModuleObjectMapperProvider().getContext(getClass());
  }

  @AfterEach
  public void tearDown() throws Exception {
    super.tearDown();
  }

  @Test
  public void testGet_players() {
    rw.writePlayer(new Player("TestPlayer3", "testPlayer3"));
    rw.writePlayer(new Player("TestPlayer4", "testPlayer4"));

    Response response = target(PlayersService.PLAYER_SERVICE_PATH)
        .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8").get();

    assertEquals(200, response.getStatus());

    try {
      List<Player> players = mapper.readValue(response.readEntity(String.class),
          mapper.getTypeFactory().constructCollectionType(List.class, Player.class));

      assertTrue(players.size() > 0);
      assertFalse(players.stream().noneMatch(c -> c.getId().equals("testPlayer3")));
      assertFalse(players.stream().noneMatch(c -> c.getId().equals("testPlayer4")));
    } catch (JsonProcessingException e) {
      fail(e.getMessage());
    }

    Paths.get(System.getProperty("user.home"), "frisbeegolf", "players", "testPlayer3.json")
        .toFile().delete();
    Paths.get(System.getProperty("user.home"), "frisbeegolf", "players", "testPlayer4.json")
        .toFile().delete();
  }

  @Test
  public void testGet_player() {
    rw.writePlayer(new Player("TestPlayer2", "testPlayer2"));

    Response response = target(PlayersService.PLAYER_SERVICE_PATH).path("testPlayer2")
        .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8").get();

    assertEquals(200, response.getStatus());

    try {
      Player player = mapper.readValue(response.readEntity(String.class), Player.class);

      assertEquals("testPlayer2", player.getId());
      assertEquals("TestPlayer2", player.getName());
    } catch (JsonProcessingException e) {
      fail(e.getMessage());
    }

    Paths.get(System.getProperty("user.home"), "frisbeegolf", "players", "testPlayer2.json")
        .toFile().delete();
  }

  @Test
  public void testPut_player() {
    Player savedPlayer = new Player("TestPlayer1", "testPlayer1");
    try {
      String json = mapper.writeValueAsString(savedPlayer);
      Response response = target(PlayersService.PLAYER_SERVICE_PATH).path("testPlayer1")
          .request(MediaType.APPLICATION_JSON + ";" + MediaType.CHARSET_PARAMETER + "=UTF-8")
          .put(Entity.json(json));

      assertEquals(200, response.getStatus());

      Player player = rw.readPlayer("testPlayer1");

      assertEquals("testPlayer1", player.getId());
      assertEquals("TestPlayer1", player.getName());

      Paths.get(System.getProperty("user.home"), "frisbeegolf", "players", "testPlayer1.json")
          .toFile().delete();
    } catch (JsonProcessingException e) {
    }
  }
}
