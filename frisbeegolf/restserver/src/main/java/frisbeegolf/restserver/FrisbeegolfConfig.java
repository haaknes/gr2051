package frisbeegolf.restserver;

import frisbeegolf.restapi.CoursesService;
import frisbeegolf.restapi.PlayersService;
import frisbeegolf.restapi.ScorecardsService;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class FrisbeegolfConfig extends ResourceConfig {

  /**
   * Initializes the FrisbeegolfConfig.
   */
  public FrisbeegolfConfig() {
    register(CoursesService.class);
    register(ScorecardsService.class);
    register(PlayersService.class);
    register(FrisbeegolfModuleObjectMapperProvider.class);
    register(JacksonFeature.class);
  }

}
