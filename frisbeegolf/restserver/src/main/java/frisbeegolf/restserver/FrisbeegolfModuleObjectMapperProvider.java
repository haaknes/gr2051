package frisbeegolf.restserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import frisbeegolf.json.FrisbeegolfModule;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FrisbeegolfModuleObjectMapperProvider implements ContextResolver<ObjectMapper> {

  private final ObjectMapper objectMapper;

  public FrisbeegolfModuleObjectMapperProvider() {
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new FrisbeegolfModule());
  }

  @Override
  public ObjectMapper getContext(final Class<?> type) {
    return objectMapper;
  }
}
