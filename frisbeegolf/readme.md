[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.idi.ntnu.no/#https://gitlab.stud.idi.ntnu.no/it1901/groups-2020/gr2051/frisbeegolf) 

# Frisbeegolf

Brukerhistoreiene finnes [her](brukerhistorier.md)

Innholdet i dette prosjektet er en app som skal holde oversikt over poengscore til ulike brukere
som spiller frisbeegolf. Det er mulig å ha flere spillere på samme spill, slik at det kun er 
en spiller som trenger å skrive ned hvor mange kast man bruker og dermed unngå at alle er på 
mobilen hele tiden. Videre er det være mulig å se en oversikt over poengscore
i form av et scorecard. Det er mulig å velge å se sine egne poengscorer baner som man har spilt på. 
Det er også mulig å legge inn egne baner slik at man kan opprette en bane og spille på denne 
dersom den ikke er lagt inn fra før av.
Man kan også gå inn på kart for å se hvor det finnes ulike baner og informasjon om disse, samt velge 
en bane fra kartet som man ønsker å spille.  

| Hovedskjerm | Spillere| Spill | Tidligere spill | Kart |
| ------ | ------ | ------ | ------ | ------ |
| ![Hovedskjerm](../images/Frisbeegolf-1.png "Hovedskjerm") | ![Spillere](../images/Frisbeegolf-5.png "Spillere") |![Spill](../images/Frisbeegolf-2.png "Spill") | ![Highscore](../images/Frisbeegolf-3.png "Highscore") | ![Kart](../images/Frisbeegolf-4.png "Kart") |
# Implisitt lagring
Vi har bestemt oss for å benytte implisitt lagring i vår applikasjon. Grunnen til dette er at prosjektet vårt skal fungere som en mobil-app, og vi ønsker ikke at brukeren
må gå aktivt inn for å lagre poengkortene, men at appen skal håndtere alt selv. Poengkortene skal derfor lagres automatisk i filer som ikke er direkte tilgjengelige for 
brukeren uten at de går gjennom appen vår.

# Pakkediagram 
Fxui-modulen inneholder brukergrensesnittet, og core-modulen inneholder domenelogikken og persistenslaget.

![Pakkediagram](../images/pakkediagram.png)

## Klassediagram
Grunnklassene til prosjektet. Scorecard klassen inneholder både en "player" og en liste over "playersInGame". Dette muligjør enkel henting av spillerkort til én gitt spiller, 
samtidig som at alle andre spillere involvert i samme spill blir lagret. Listen brukes videre for å kunne hente alle spillerne som spilte samtidig sine unike spillerkort.

![Klassediagram](../images/klassediagram.png)

## Sekvensdiagram
Sekvensdiagrammet beskriver et viktig brukstilfelle der spilleren starter å spille et spill, for å så se på oversikten over alle spillene sine.
Diagrammet er basert på SimulateUserIT, der spilleren starter et spill med Per, kaster en gang, og avslutter. Noen parametere i funksjoner er abstrahert bort
for å gjøre det mer oversiktlig.

![Sekvensdiagram](../images/sekvensdiagram.png)
