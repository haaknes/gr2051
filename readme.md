[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.idi.ntnu.no/#https://gitlab.stud.idi.ntnu.no/haaknes/gr2051) 

# Group gr2051 repository
Applikasjonen bygges ved å kjøre *mvn install*. Deretter startes serveren ved å 
 kjøre *mvn -pl integrationtests jetty:run* og brukergrensesnittet ved 
 *mvn -pl fxui javafx:run* i to forskjellige terminaler.

Ettersom FXUI-tester feiler av og til grunnet parallellbehandling av tråder
 har vi valgt at applikasjonen bygges selv om noen tester feiler.  


*jacoco* for restapi ligger under **restserver/target/site/jacoco-aggregate/index.html**

## Kodestruktur

- **frisbeegolf** innholder kodingsprosjektet
  - **core** modul for domenelogikken og persistens (JSON)
    - **src/main**
      - **java** inneholder koden til domenelogikken og persistens
      - **resources** inneholder ressurser som feks. eksempelfiler av baner
    - **src/test**
      - **java** innholder testene til domenelogikken og persistens
      - **resources** inneholder ressurser som brukes til å teste domenelogikken og persistensen
  - **fxui** modul for brukergrensesnittet (JavaFX)
    - **src/main**
      - **java** inneholder koden til brukergrensesnittet
      - **resources** inneholder FXML-filer
    - **src/test** 
      - **java** innholder testene til brukergrensesnittet
      - **resources** inneholder FXML-filer for testene
  - **integrationtests** modul for å koble sammen grensesnittet og REST
    - **src/main** inneholder konfigurasjon av restserveren for oppstart av denne
    - **src/test** inneholder testene for sammenkobling av REST og grensesnittet
  - **restapi** modul for REST-API
    - **src/main** inneholder koden til REST-API
  - **restserver** modul for REST-server
    - **src/main** inneholder koden for oppsett av server
    - **src/test** inneholder testene til REST-API som bruker serveren
         
- **.theia** inneholder gitpod-spesifikk info
- **images** inneholder div bilder og diagrammer som brukes i README-filer
